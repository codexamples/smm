<?php

namespace Tests;
use App\Models\CallCenter;
use App\Models\CallForm;

/**
 * Использование тестовых сущностей в звонилке
 */
trait UsesCallerTestModels
{
    /**
     * Создает тестовый колцентр, если такового нет
     *
     * @return \App\Models\CallCenter
     */
    protected function makeCallCenter()
    {
        $center = CallCenter::where('name', 'test-center')->where('phone', '97979797979')->first();

        if (!$center) {
            $center = CallCenter::create([
                'name' => 'test-center',
                'phone' => '97979797979',
                'msgfilename' => null,
                'works_from' => '12:00',
                'works_to' => '21:20',
                'works_weekends' => true,
                'client_mails' => 'asergeev@digital-mind.ru',
                'works_weekends_from' => '12:20',
                'works_weekends_to' => '21:00',
            ]);
        }

        return $center;
    }

    /**
     * Создает тестовую facebook-форму, если таковой нет
     *
     * @return \App\Models\CallForm
     */
    protected function makeCallForm()
    {
        $center = $this->makeCallCenter();
        $form = CallForm::where('name', 'test-form')->where('facebook_id', '1005001488')->first();

        if (!$form) {
            $form = CallForm::create([
                'center_id' => $center->id,
                'facebook_id' => '1005001488',
                'name' => 'test-form',
                'mail_only' => true,
                'from_mail' => 'test@test.test',
                'from_name' => 'Test The Test',
            ]);
        }

        return $form;
    }

    /**
     * Создает тестовую запись лога звонилки, если таковой нет
     *
     * @param  string  $status_name
     * @param  array  $status_data
     * @return \App\Models\CallLog
     */
    protected function makeCallLog(string $status_name, array $status_data)
    {
        return $this->makeCallForm()->appendLog('94949494949', [
            'page_name' => 'Test Page',
            'full_name' => 'Ushat Pomoev',
        ], $status_name, $status_data);
    }
}
