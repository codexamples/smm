<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\UsesCallerTestModels;

class CallApiTest extends TestCase
{
    use UsesCallerTestModels;

    /**
     * Route: /api/call/generate
     * - код ответа = 200
     * - тело ответа = "1"
     *
     * @return void
     */
    public function testGenerateRoute()
    {
        $form = $this->makeCallForm();
        $response = $this->call('POST', '/api/call/generate', [], [], [], [], json_encode([
            'token' => config('caller.auth.api_token'),
            'form_id' => $form->facebook_id,
            'phone_number' => '91005001488',
            'wants_call' => 'yes',
            'full_name' => 'Kamaz Navoza',
        ]));

        $response->assertStatus(200);
        $this->assertEquals('1', $response->baseResponse->getContent());
    }

    /**
     * Route: /api/call/end
     * - код ответа = 200
     * - тело ответа = "1"
     *
     * @return void
     */
    public function testEndRoute()
    {
        $log = $this->makeCallLog('talking', []);
        $causes = [0, 16, 17, 19];
        $response = $this->call('GET', '/api/call/end', [
            'ID' => $log->id,
            'HANGUPCAUSE' => $causes[rand(0, count($causes) - 1)],
            'direction' => 'center',
        ]);

        $response->assertStatus(200);
        $this->assertEquals('1', $response->baseResponse->getContent());
    }
}
