<?php

namespace Tests\Unit;

use Exception;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Tests\UsesCallerTestModels;

class CallerConsoleTest extends TestCase
{
    use UsesCallerTestModels;

    /**
     * Command: call:back
     * - выполняется без ошибок
     *
     * @return void
     */
    public function testCallBack()
    {
        try {
            $this->makeCallLog('hangup', [
                'state' => 16,
                'direction' => 1,
            ]);
            $this->makeCallLog('talking', []);
            $this->makeCallLog('scheduled', []);

            Artisan::call('call:back');
            $error = '';
        } catch (Exception $ex) {
            $error = $ex->getMessage();
        }

        $this->assertEquals('', $error);
    }
}
