<?php

namespace Tests\Unit;

use App\Business\Api\Asterisk;
use App\Business\Mail\CallerMailer;
use Exception;
use Tests\TestCase;
use Tests\UsesCallerTestModels;

class CallerTest extends TestCase
{
    use UsesCallerTestModels;

    /**
     * CallerMailer::sendLead
     * - отдает true
     *
     * @return void
     */
    public function testLeadSendsViaMailGun()
    {
        /** @var  \App\Business\Mail\CallerMailer  $mailer */
        $mailer = app()->make(CallerMailer::class);

        $result = $mailer->sendLead('unit@test.php', 'Unit Test', '***@***.ru', 'TEST', [
            'page_name' => 'Unit Page',
            'form_name' => 'Unit Form',
            'full_name' => 'Rulon Oboev',
            'phone_number' => '88005553535',
            'event' => 'Prosche pozvonit\' chem u kogo-to zanimat\'',
        ]);

        $this->assertEquals(true, $result);
    }

    /**
     * Asterisk::generateCall
     * - не бросает исключений
     *
     * @return void
     */
    public function testCallGeneratesByAsterisk()
    {
        /** @var  \App\Business\Api\Asterisk  $asterisk */
        $asterisk = app()->make(Asterisk::class);

        try {
            $log = $this->makeCallLog('pending', []);
            $asterisk->generateCall('91271271271', '92552552552', config('caller.call.msgfilename'), $log->id, 5);
            $error = '';
        } catch (Exception $ex) {
            $error = $ex->getMessage();
        }

        $this->assertEquals('', $error);
    }
}
