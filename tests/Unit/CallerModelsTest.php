<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use Tests\UsesCallerTestModels;

class CallerModelsTest extends TestCase
{
    use UsesCallerTestModels;

    /**
     * CallCenter::isInWorkingTimeNow
     * - правильно вычисляется рабочее время
     *
     * @return void
     */
    public function testCallCenterWorkingTime()
    {
        $center = $this->makeCallCenter();
        $now = new Carbon();

        $now->year = 2017;
        $now->month = 6;
        $now->day = 22; // weekdays : thursday
        $now->second = 0;

        $now->hour = 11;
        $now->minute = 59;
        $this->assertEquals(false, $center->isInWorkingTimeNow($now));

        $now->hour = 14;
        $now->minute = 1;
        $this->assertEquals(true, $center->isInWorkingTimeNow($now));

        $now->hour = 21;
        $now->minute = 21;
        $this->assertEquals(false, $center->isInWorkingTimeNow($now));

        $now->day = 24; // weekends : saturday

        $now->hour = 12;
        $now->minute = 19;
        $this->assertEquals(false, $center->isInWorkingTimeNow($now));

        $now->hour = 15;
        $now->minute = 30;
        $this->assertEquals(true, $center->isInWorkingTimeNow($now));

        $now->hour = 21;
        $now->minute = 1;
        $this->assertEquals(false, $center->isInWorkingTimeNow($now));
    }
}
