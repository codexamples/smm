<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefuckCallCenterMsgFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('call_centers', function (Blueprint $table) {
            $table->dropColumn('msgfilename', 'msgfilepath');
        });

        Schema::table('call_centers', function (Blueprint $table) {
            $table->string('msg_file_hash')->nullable();
        });

        Schema::table('call_centers', function (Blueprint $table) {
            $table->foreign('msg_file_hash')->references('hash')->on('msg_files')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('call_centers', function (Blueprint $table) {
            $table->dropForeign('call_centers_msg_file_hash_foreign');
        });

        Schema::table('call_centers', function (Blueprint $table) {
            $table->dropColumn('msg_file_hash');
        });

        Schema::table('call_centers', function (Blueprint $table) {
            $table->string('msgfilename')->nullable();
            $table->string('msgfilepath')->nullable();
        });
    }
}
