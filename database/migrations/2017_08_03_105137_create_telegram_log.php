<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelegramLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chat_id')->nullable();
            $table->string('telegram_id');
            $table->string('telegram_name');
            $table->unsignedInteger('form_id')->nullable();
            $table->string('center_name');
            $table->string('form_name');
            $table->text('lead_data');
            $table->text('error');
            $table->timestamps();

            $table->foreign('chat_id')->references('id')->on('telegram_chats')->onDelete('set null');
            $table->foreign('form_id')->references('id')->on('call_forms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_logs');
    }
}
