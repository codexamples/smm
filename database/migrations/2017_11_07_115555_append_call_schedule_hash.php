<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppendCallScheduleHash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('call_schedules', function (Blueprint $table) {
            $table->string('hash');
        });

        DB::table('call_schedules')->update([
            'hash' => md5(microtime()),
        ]);

        Schema::table('call_schedules', function (Blueprint $table) {
            $table->dropPrimary('lead_phone');
        });

        Schema::table('call_schedules', function (Blueprint $table) {
            $table->primary('hash');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('call_schedules', function (Blueprint $table) {
            $table->dropPrimary('hash');
        });

        Schema::table('call_schedules', function (Blueprint $table) {
            $table->primary('lead_phone');
        });

        Schema::table('call_schedules', function (Blueprint $table) {
            $table->dropColumn('hash');
        });
    }
}
