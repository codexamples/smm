<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('form_id')->nullable();
            $table->string('center_name');
            $table->string('form_name');
            $table->string('lead_phone');
            $table->text('facebook_data');
            $table->string('status_name');
            $table->text('status_data');
            $table->boolean('call_back');
            $table->timestamp('frozen_at');
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('call_forms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_logs');
    }
}
