<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallFormTelegramChat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_form_telegram_chat', function (Blueprint $table) {
            $table->unsignedInteger('form_id');
            $table->string('chat_id');
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('call_forms')->onDelete('cascade');
            $table->foreign('chat_id')->references('id')->on('telegram_chats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_form_telegram_chat');
    }
}
