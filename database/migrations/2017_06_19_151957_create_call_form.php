<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id');
            $table->string('facebook_id')->unique();
            $table->string('name');
            $table->boolean('mail_only')->default(false);
            $table->string('from_mail')->nullable();
            $table->string('from_name')->nullable();
            $table->timestamps();

            $table->foreign('center_id')->references('id')->on('call_centers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_forms');
    }
}
