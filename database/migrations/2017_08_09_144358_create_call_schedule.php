<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_schedules', function (Blueprint $table) {
            $table->string('lead_phone');
            $table->unsignedInteger('form_id');
            $table->text('facebook_data');
            $table->string('type');
            $table->unsignedTinyInteger('call_back_count');
            $table->dateTime('call_back_at');
            $table->timestamps();

            $table->primary('lead_phone');
            $table->foreign('form_id')->references('id')->on('call_forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_schedules');
    }
}
