<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppendCallCenterCallbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('call_centers', function (Blueprint $table) {
            $table->boolean('calls_back')->default(false);
            $table->unsignedTinyInteger('calls_back_times')->nullable();
            $table->unsignedSmallInteger('calls_back_interval')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('call_centers', function (Blueprint $table) {
            $table->dropColumn('calls_back', 'calls_back_times', 'calls_back_interval');
        });
    }
}
