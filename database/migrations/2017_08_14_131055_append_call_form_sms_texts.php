<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppendCallFormSmsTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('call_forms', function (Blueprint $table) {
            $table->string('sms_sender')->nullable();
            $table->string('sms_text_simple')->nullable();
            $table->string('sms_text_schedule')->nullable();
            $table->string('sms_text_time')->nullable();
            $table->string('sms_text_time_invalid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('call_forms', function (Blueprint $table) {
            $table->dropColumn(
                'sms_sender',
                'sms_text_simple',
                'sms_text_schedule',
                'sms_text_time',
                'sms_text_time_invalid'
            );
        });
    }
}
