<?php

use Illuminate\Database\Seeder;
use App\Models\User as User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'login' => '***',
            'password' => Hash::make('***'),
            'name' => '***',
        ]);

        User::create([
            'login' => '***',
            'password' => Hash::make('***'),
            'name' => '***',
        ]);
    }
}
