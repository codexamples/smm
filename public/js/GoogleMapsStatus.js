var GoogleMaps = {

};

GoogleMaps.ajax = function () {
   $.ajax({
      dataType : 'json',
      type: 'POST',
      url  : '/api/parser/status',
      success : GoogleMaps.finish,
      error : GoogleMaps.error
   })
};

GoogleMaps.finish = function (data) {
   for (var i=0; i<data.length; i++) {
      //Выделяет кнопку
      if (data[i].status === 'complete') {
         $('#downloadButton'+data[i].id).text('Завершено');
         $('#downloadButton'+data[i].id).removeClass('btn-default');
         $('#downloadButton'+data[i].id).addClass('btn-primary');

         str = data[i].output_path;
          $("#downloadButton"+data[i].id).attr('href', 'download/'+data[i].id);
          $('#status'+data[i].id).text('Выполнено');
          $("#downloadButton"+data[i].id).removeAttr('disabled');
      } else {
          $('#downloadButton'+data[i].id).attr('href', '#');
      }
   }
};

GoogleMaps.error = function (data) {
  // console.log(data);
};

$(document).ready(function() {
   setInterval(GoogleMaps.ajax, 250);
});
