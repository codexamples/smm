<?php

namespace App\Http\Requests;

class CallFormCrudRequest extends NonAuthableRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'facebook_id' => 'required|max:32|unique:call_forms,facebook_id,' . $this->id . ',id',
            'name' => 'required|max:64',
            'center_id' => 'required|integer|min:1',
            'mail_only' => 'required|integer|between:0,1',
            'from_mail' => 'nullable|email',
            'from_name' => 'nullable|between:3,32',
            'chats.*' => 'required',
            'sms_sender' => 'nullable|max:32',
            'sms_text_simple' => 'nullable|max:191',
            'sms_text_schedule' => 'nullable|max:191',
            'sms_text_time' => 'nullable|max:191',
            'sms_text_time_invalid' => 'nullable|max:191',
        ];
    }
}
