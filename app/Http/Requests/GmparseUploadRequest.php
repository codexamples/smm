<?php

namespace App\Http\Requests;

class GmparseUploadRequest extends NonAuthableRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:txt|max:1024',
        ];
    }
}
