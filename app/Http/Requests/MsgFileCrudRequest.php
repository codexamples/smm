<?php

namespace App\Http\Requests;

class MsgFileCrudRequest extends NonAuthableRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:64',
            'file' => ($this->hash ? 'nullable' : 'required') . '|mimes:wav|max:10240',
        ];
    }
}
