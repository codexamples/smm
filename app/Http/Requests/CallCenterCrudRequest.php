<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CallCenterCrudRequest extends NonAuthableRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:64|unique:call_centers,name,' . $this->id . ',id',
            'phone' => 'required|numeric|unique:call_centers,phone,' . $this->id . ',id',
            'msg_file_hash' => 'nullable|max:64',
            'works_from' => 'nullable|date_format:H:i',
            'works_to' => 'nullable|date_format:H:i',
            'works_weekends' => 'required|integer|between:0,1',
            'works_weekends_from' => 'nullable|date_format:H:i',
            'works_weekends_to' => 'nullable|date_format:H:i',
            'client_mails' => 'between:0,191',
            'calls_back' => 'required|integer|between:0,1',
            'calls_back_times' => 'nullable|integer|between:1,10',
            'calls_back_interval' => 'nullable|integer|between:5,300',
            'msg_delay' => 'required|integer|between:0,60',
        ];
    }

    /**
     * Перезаписывает данные, возвращает самого себя
     *
     * @param  array  &$all
     * @return static
     */
    protected function mergeThis(array &$all)
    {
        $this->merge($all);
        return $this;
    }

    /**
     * Валидирует данные по рабочему времени колцентра
     *
     * @return static
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateWorkFork()
    {
        $errors = [];
        $all = $this->all();

        $this->validateWorkForkA($errors, $all, '', '');
        $this->validateWorkForkA($errors, $all, '_weekends', ' (вых)');

        if (empty($errors)) {
            $this->validateWorkForkB($errors, $all, '', '');
            $this->validateWorkForkB($errors, $all, '_weekends', ' (вых)');
        }

        if (count($errors)) {
            /** @var  \Illuminate\Validation\Validator  $validator */
            $validator = Validator::make([], []);

            foreach ($errors as $index => $error) {
                $validator->messages()->add('err_' . $index, $error);
            }

            throw new ValidationException($validator);
        }

        return $this->mergeThis($all);
    }

    /**
     * Первый этап валидации рабочего времени
     *
     * @param  array  &$errors
     * @param  array  &$all
     * @param  string  $key
     * @param  string  $title
     * @return void
     */
    private function validateWorkForkA(array &$errors, array &$all, string $key, string $title)
    {
        if ($this->has('works' . $key . '_from')) {
            $all['works' . $key . '_from'] = strtotime($this->input('works' . $key . '_from'));

            if ($all['works' . $key . '_from'] === false) {
                $errors[] = 'Неверное время начала работы' . $title;
            }
        }

        if ($this->has('works' . $key . '_to')) {
            $all['works' . $key . '_to'] = strtotime($this->input('works' . $key . '_to'));

            if ($all['works' . $key . '_to'] === false) {
                $errors[] = 'Неверное время конца работы' . $title;
            }
        }
    }

    /**
     * Второй этап валидации рабочего времени
     *
     * @param  array  &$errors
     * @param  array  &$all
     * @param  string  $key
     * @param  string  $title
     * @return void
     */
    private function validateWorkForkB(array &$errors, array &$all, string $key, string $title)
    {
        if ($all['works' . $key . '_from'] && $all['works' . $key . '_to']) {
            if ($all['works' . $key . '_from'] >= $all['works' . $key . '_to']) {
                $errors[] = 'Время начала больше времени конца работы' . $title;
            } else {
                $all['works' . $key . '_from'] = date('H:i:00', $all['works' . $key . '_from']);
                $all['works' . $key . '_to'] = date('H:i:00', $all['works' . $key . '_to']);
            }
        } else {
            $all['works' . $key . '_from'] = null;
            $all['works' . $key . '_to'] = null;
        }
    }
}
