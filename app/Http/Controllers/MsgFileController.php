<?php

namespace App\Http\Controllers;

use App\Business\Ftp\MsgFileClient as MsgFileFtpClient;
use App\Http\Requests\MsgFileCrudRequest;
use App\Models\MsgFile;

class MsgFileController extends Controller
{
    /**
     * Показывает список всех отбивочных файлов
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.msg_file.index', [
            'files' => MsgFile::paginate(25),
        ]);
    }

    /**
     * Показывает форму для создания отбивочного файла
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main.msg_file.edit', [
            'file' => null,
        ]);
    }

    /**
     * Показывает форму для редактирования отбивочного файла
     *
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function edit(string $hash)
    {
        return view('main.msg_file.edit', [
            'file' => MsgFile::findOrFail($hash),
        ]);
    }

    /**
     * Создает новый отбивочный файл
     *
     * @param  \App\Business\Ftp\MsgFileClient  $ftp
     * @param  \App\Http\Requests\MsgFileCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MsgFileFtpClient $ftp, MsgFileCrudRequest $request)
    {
        $all = $request->all();
        $all['hash'] = md5(microtime());
        $all['path'] = 'msg_sound/' . $all['hash'] . '.wav';

        /** @var  \App\Models\MsgFile  $file */
        $file = MsgFile::create($all);

        return redirect()
            ->route('msg_file.edit', ['hash' => $file->uploadReal($request->file('file'), $ftp)->hash])
            ->with('msg', 'Отбивка создана');
    }

    /**
     * Обновляет отбивочный файл
     *
     * @param  \App\Business\Ftp\MsgFileClient  $ftp
     * @param  \App\Http\Requests\MsgFileCrudRequest  $request
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function update(MsgFileFtpClient $ftp, MsgFileCrudRequest $request, string $hash)
    {
        /** @var  \App\Models\MsgFile  $file */
        $file = MsgFile::findOrFail($hash);
        $file->fill($request->all())->save();

        return redirect()
            ->route('msg_file.edit', ['hash' => $file->uploadReal($request->file('file'), $ftp)->hash])
            ->with('msg', 'Отбивка обновлена');
    }

    /**
     * Удаляет отбивочный файл
     *
     * @param  \App\Business\Ftp\MsgFileClient  $ftp
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     */
    public function destroy(MsgFileFtpClient $ftp, string $hash)
    {
        /** @var  \App\Models\MsgFile  $file */
        $file = MsgFile::findOrFail($hash);
        $file->unlinkReal($ftp)->delete();

        return redirect()->route('msg_file.index')->with('msg', 'Отбивка удалена');
    }
}
