<?php

namespace App\Http\Controllers;

use App\Business\Api\GoogleMapsCoordinates;
use App\Models\Gmparse;
use App\Http\Requests\GmparseUploadRequest;
use App\Jobs\Gmparser;

class GmparseController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.gmparse.index');
    }

    /**
     * @param $gm_api \App\Business\Api\GoogleMapsCoordinates
     * @param $request \App\Http\Requests\GmparseUploadRequest
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(GoogleMapsCoordinates $gm_api, GmparseUploadRequest $request)
    {
        $file = $request->file('file');

        $s = file_get_contents($file->getRealPath());
        $s = str_replace("\r\n", "\n", $s);
        $s = str_replace("\r", "\n", $s);

        $gmparse = Gmparse::create([
            'name' => pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME),
            'lines_in' => $s,
            'lines_out' => '',
            'status' => '',
        ]);

        $this->dispatch(new Gmparser($gm_api, $gmparse->id));
        return redirect()->route('gmparse.status')->with('status', 'Файл загружен и обрабатывается');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function status()
    {
        return view('main.gmparse.status', [
            'gmparses' => Gmparse::orderBy('id', 'desc')->paginate(25),
        ]);
    }

    /**
     * @param $id int
     *
     * @return \Illuminate\Http\Response
     */
    public function download(int $id)
    {
        $gmparse = Gmparse::findOrFail($id);

        return response()->make(str_replace("\n", "\r\n", $gmparse->lines_out), 200, [
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="' . $gmparse->name . '.txt"',
        ]);
    }

    /**
     * @param $gm_api \App\Business\Api\GoogleMapsCoordinates
     * @param $id int
     *
     * @return \Illuminate\Http\Response
     */
    public function retry(GoogleMapsCoordinates $gm_api, int $id)
    {
        $gmparse_old = Gmparse::findOrFail($id);
        $gmparse = Gmparse::create([
            'name' => $gmparse_old->name,
            'lines_in' => $gmparse_old->lines_in,
            'lines_out' => $gmparse_old->lines_out,
            'status' => $gmparse_old->status,
        ]);

        $gmparse_old->delete();
        $this->dispatch(new Gmparser($gm_api, $gmparse->id));

        return redirect()->route('gmparse.status')->with('status', 'Обработка файла перезапущена');
    }

    /**
     * @param $id int
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        Gmparse::findOrFail($id)->delete();
        return redirect()->route('gmparse.status')->with('status', 'Файл удален');
    }
}
