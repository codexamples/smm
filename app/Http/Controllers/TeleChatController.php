<?php

namespace App\Http\Controllers;

use App\Models\TelegramChat;

class TeleChatController extends Controller
{
    /**
     * Показывает список всех чат-комнат Telegram
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.tele_chat.index', [
            'chats' => TelegramChat::orderBy('created_at', 'desc')->paginate(25),
        ]);
    }

    /**
     * Показывает деталку чат-комнаты Telegram
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view(int $id)
    {
        return view('main.tele_chat.view', [
            'chat' => TelegramChat::findOrFail($id),
        ]);
    }
}
