<?php

namespace App\Http\Controllers;

use App\Models\TelegramLog;

class TeleLogController extends Controller
{
    /**
     * Показывает журнал по чат-комнатам Telegram
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.tele_log', [
            'logs' => TelegramLog::orderBy('created_at', 'desc')->paginate(25),
        ]);
    }
}
