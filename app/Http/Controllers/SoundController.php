<?php

namespace App\Http\Controllers;

use App\Models\CallCenter;
use App\Models\CallLog;
use App\Models\MsgFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SoundController extends Controller
{
    /**
     * Отдает звуковой файл на воспроизведение, связанный с лог-записью журнала звонков
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function record(int $id)
    {
        /** @var  \App\Models\CallLog  $log */
        $log = CallLog::findOrFail($id);

        return $this->giveSound($log->record_path);
    }

    /**
     * Отдает звуковой файл на воспроизведение, связанный с отбивкой колцентра
     *
     * @param  string  $hash
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function msgFile(string $hash)
    {
        /** @var  \App\Models\MsgFile  $file */
        $file = MsgFile::findOrFail($hash);

        return $this->giveSound($file->path);
    }

    /**
     * Отдает звуковой файл на воспроизведение по указанному пути
     *
     * @param  string|null  $path
     * @return \Illuminate\Http\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function giveSound($path)
    {
        $path = storage_path($path ? $path : 'unknown.file');

        if (file_exists($path)) {
            return response()->download($path);
        }

        throw new NotFoundHttpException();
    }
}
