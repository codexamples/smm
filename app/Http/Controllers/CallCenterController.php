<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallCenterCrudRequest;
use App\Models\CallCenter;
use App\Models\MsgFile;

class CallCenterController extends Controller
{
    /**
     * Показывает список всех колцентров
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.call_center.index', [
            'centers' => CallCenter::paginate(25),
        ]);
    }

    /**
     * Показывает форму для создания колцентра
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main.call_center.edit', [
            'msg_files' => MsgFile::all(),
            'center' => null,
        ]);
    }

    /**
     * Показывает форму для редактирования колцентра
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        return view('main.call_center.edit', [
            'msg_files' => MsgFile::all(),
            'center' => CallCenter::findOrFail($id),
        ]);
    }

    /**
     * Создает новый колцентр
     *
     * @param  \App\Http\Requests\CallCenterCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CallCenterCrudRequest $request)
    {
        /** @var  \App\Models\CallCenter  $center */
        $center = CallCenter::create(
            $request->validateWorkFork()->all()
        );

        return redirect()->route('call_center.edit', ['id' => $center->id])->with('msg', 'Колцентр создан');
    }

    /**
     * Обновляет колцентр
     *
     * @param  \App\Http\Requests\CallCenterCrudRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CallCenterCrudRequest $request, int $id)
    {
        /** @var  \App\Models\CallCenter  $center */
        $center = CallCenter::findOrFail($id);

        $center->fill(
            $request->validateWorkFork()->all()
        )->save();

        return redirect()->route('call_center.edit', ['id' => $center->id])->with('msg', 'Колцентр обновлен');
    }

    /**
     * Удаляет колцентр
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        /** @var  \App\Models\CallCenter  $center */
        $center = CallCenter::findOrFail($id);
        $center->delete();

        return redirect()->route('call_center.index')->with('msg', 'Колцентр удален');
    }
}
