<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->redirectTo = route('index');
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * @var string
     */
    protected $redirectTo;

    /**
     * @return string
     */
    public function username()
    {
        return 'login';
    }
}
