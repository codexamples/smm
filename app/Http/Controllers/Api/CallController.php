<?php

namespace App\Http\Controllers\Api;

use App\Business\Caller\Caller;
use App\Models\CallForm;
use App\Models\CallLog;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CallController extends Controller
{
    /**
     * Генерирует звонок с facebook-форм
     *
     * @param  \App\Business\Caller\Caller  $caller
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Caller $caller, Request $request)
    {
        Log::info('==> Call/Generate start');
        $args = json_decode($request->getContent(), true);

        try {
            if ($args['token'] !== config('caller.auth.api_token')) {
                throw new Exception('Invalid token');
            }

            $form = CallForm::where('facebook_id', $args['form_id'])->first();

            if (!$form) {
                throw new Exception('Form not found');
            }

            $caller->generateCall($form, $args, true);

            Log::info('==> Call/Generate ok');
            $success = 1;
        } catch (Exception $ex) {
            Log::info('==> Call/Generate error: ' . $ex->getMessage());
            $success = 0;
        }

        return response()->make($success);
    }

    /**
     * Принимает оповещение от телефонии об окончании звонка
     *
     * @param  \App\Business\Caller\Caller  $caller
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function end(Caller $caller, Request $request)
    {
        Log::info('==> Call/End start');

        try {
            $log = CallLog::whereNotNull('form_id')->where('id', $request->input('ID'))->first();

            if (!$log) {
                throw new Exception('Log not found');
            }

            switch ($request->input('direction')) {
                case 'center':
                    $direction = -1;
                    break;

                case 'client':
                    $direction = 1;
                    break;

                default:
                    $direction = 0;
                    break;
            }

            $state = intval($request->input('HANGUPCAUSE'));

            if ($state === 16) {
                switch ($request->input('DIALSTATUS')) {
                    case 'BUSY':
                        $state = 17;
                        break;

                    case 'NOANSWER':
                        $state = 19;
                        break;

                    case 'CANCEL':
                        $state = 0;
                        break;

                    case 'CONGESTION':
                        $state = 34;
                        break;

                    case 'CHANUNAVAIL':
                        $state = 38;
                        break;
                }
            }

            $caller->endCall($log, $state, $direction);

            Log::info('==> Call/End ok');
            $success = 1;
        } catch (Exception $ex) {
            Log::info('==> Call/End error: ' . $ex->getMessage());
            $success = 0;
        }

        return response()->make($success);
    }

    /**
     * Принимает оповещение о новой записи разговора, с данными о принадлежности тому или иному разговору
     *
     * @param  \App\Business\Caller\Caller  $caller
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function record(Caller $caller, Request $request)
    {
        Log::info('==> Call/Record start');

        try {
            $log = CallLog::whereNotNull('form_id')->where('id', $request->input('ID'))->first();

            if (!$log) {
                throw new Exception('Log not found');
            }

            $caller->saveRecordSound($log, $request->input('name'), intval($request->input('duration')));

            Log::info('==> Call/Record ok');
            $success = 1;
        } catch (Exception $ex) {
            Log::info('==> Call/Record error: ' . $ex->getMessage());
            $success = 0;
        }

        return response()->make($success);
    }
}
