<?php

namespace App\Http\Controllers\Api;

use App\Models\Gmparse;

class GmparseController extends Controller
{
    /**
     * @param $id int
     *
     * @return \Illuminate\Http\Response
     */
    public function status(int $id)
    {
        $status = Gmparse::findOrFail($id)->status;
        $json = json_decode($status, true);

        return response()->json($json ? $json : ['raw' => $status]);
    }
}
