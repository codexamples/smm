<?php

namespace App\Http\Controllers\Api;

use App\Business\Telegram\Api as TelegramDerived;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api as TelegramParent;

class TelegramController extends Controller
{
    /**
     * @param  \Telegram\Bot\Api  $telegram
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function webhook(TelegramParent $telegram, Request $request)
    {
        $commands = $telegram->getCommands();
        $telegram = new TelegramDerived(
            $telegram->getAccessToken(),
            $telegram->isAsyncRequest(),
            $telegram->getClient()->getHttpClientHandler()
        );
        $telegram->addCommands($commands);

        try {
            Log::info('==> Telegram Webhook Data: ' . $request->getContent());

            $telegram->handleCommands(true, function ($update) use ($telegram) {
                /** @var  \Telegram\Bot\Objects\Update  $update */
                $msg = $update->getMessage();

                Log::info('==> Telegram Unrecognized Command');
                Log::info('==> Text: ' . $msg->getText());

                $chat = $msg->getChat();
                if ($chat) {
                    $telegram->sendMessage([
                        'chat_id' => $chat->getId(),
                        'text' => implode(PHP_EOL, [
                            'Простите, но я не знаю, что это:',
                            $msg->getText(),
                            '',
                            'Список доступных команд: /help',
                        ]),
                    ]);
                }
            });
        } catch (Exception $ex) {
            Log::info('==> Telegram Webhook Error: ' . $ex->getMessage());
        }

        return response()->make('ok');
    }
}
