<?php

namespace App\Http\Controllers\Api;

use App\Models\CallForm;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Отдает в JS-код разобранный шаблон SMS-текста
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSmsText(Request $request)
    {
        return response()->make(CallForm::getSmsText($request->getContent(), new Carbon()));
    }
}
