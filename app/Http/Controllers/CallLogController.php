<?php

namespace App\Http\Controllers;

use App\Models\CallForm;
use App\Models\CallLog;
use Illuminate\Http\Request;

class CallLogController extends Controller
{
    /**
     * Показывает журнал звонков
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('main.call_log', [
            'search' => $request->q,
            'forms' => CallForm::all(),
            'call_backs' => [
                '' => '&lt; Не важно &gt;',
                '0' => 'Нет',
                '1' => 'Да',
            ],
            'logs' => CallLog::search($request->q),
        ]);
    }
}
