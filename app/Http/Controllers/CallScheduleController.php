<?php

namespace App\Http\Controllers;

use App\Models\CallSchedule;
use Illuminate\Http\Request;

class CallScheduleController extends Controller
{
    /**
     * Показывает журнал звонков
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.call_schedule', [
            'schedules' => CallSchedule::orderBy('created_at', 'asc')->paginate(25),
        ]);
    }
}
