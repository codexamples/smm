<?php

namespace App\Http\Controllers;

use App\Http\Requests\CallFormCrudRequest;
use App\Models\CallCenter;
use App\Models\CallForm;
use App\Models\TelegramChat;

class CallFormController extends Controller
{
    /**
     * Показывает список всех facebook-форм
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.call_form.index', [
            'forms' => CallForm::paginate(25),
        ]);
    }

    /**
     * Показывает форму для создания facebook-формы
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main.call_form.edit', [
            'centers' => CallCenter::all(),
            'chats' => TelegramChat::all(),
            'form_chat' => is_array(old('chats')) ? old('chats') : [],
            'form' => null,
        ]);
    }

    /**
     * Показывает форму для редактирования facebook-формы
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        /** @var  \App\Models\CallForm  $form */
        $form = CallForm::findOrFail($id);

        return view('main.call_form.edit', [
            'centers' => CallCenter::all(),
            'chats' => TelegramChat::all(),
            'form_chat' => $form->chats()->pluck('id')->toArray(),
            'form' => $form,
        ]);
    }

    /**
     * Создает новую facebook-форму
     *
     * @param  \App\Http\Requests\CallFormCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CallFormCrudRequest $request)
    {
        /** @var  \App\Models\CallForm  $form */

        $form = CallForm::create($request->all());
        $form->chats()->sync($request->input('chats', []));

        return redirect()->route('call_form.edit', ['id' => $form->id])->with('msg', 'Форма создана');
    }

    /**
     * Обновляет facebook-форму
     *
     * @param  \App\Http\Requests\CallFormCrudRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CallFormCrudRequest $request, int $id)
    {
        /** @var  \App\Models\CallForm  $form */
        $form = CallForm::findOrFail($id);

        $form->fill($request->all())->save();
        $form->chats()->sync($request->input('chats', []));

        return redirect()->route('call_form.edit', ['id' => $form->id])->with('msg', 'Форма обновлена');
    }

    /**
     * Удаляет facebook-форму
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        /** @var  \App\Models\CallForm  $form */
        $form = CallForm::findOrFail($id);
        $form->delete();

        return redirect()->route('call_form.index')->with('msg', 'Форма удалена');
    }
}
