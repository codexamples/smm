<?php

namespace App\Business;

use InvalidArgumentException;

/**
 * Код для классов, использующих динамические реквизиты подключения куда-либо
 */
trait UsesCredentials
{
    /**
     * Реквизиты входа на ftp-сервер
     *
     * @var array
     */
    protected $credentials;

    /**
     * Возвращает поле-реквизит входа на ftp-сервер
     *
     * @param  string  $credential
     * @return string|int
     * @throws \InvalidArgumentException
     */
    public function __get(string $credential)
    {
        if (array_key_exists($credential, $this->credentials)) {
            return $this->credentials[$credential];
        }

        throw new InvalidArgumentException('Credential ' . $credential . ' not found');
    }
}
