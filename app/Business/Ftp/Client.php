<?php

namespace App\Business\Ftp;

use App\Business\UsesCredentials;

/**
 * Ftp-клиент
 *
 * @property-read string $host
 * @property-read string $login
 * @property-read string $password
 * @property-read int $port
 * @property-read int $timeout
 */
class Client
{
    use UsesCredentials;

    /**
     * Подключение к серверу
     *
     * @var resource|null
     */
    private $stream;

    /**
     * Создает экземпляр, подрубая подключение к ftp-серверу
     *
     * @param  string  $host
     * @param  string  $login
     * @param  string  $password
     * @param  int  $port
     * @param  int  $timeout
     */
    public function __construct(string $host, string $login, string $password, int $port = 21, int $timeout = 90)
    {
        $this->credentials = [
            'host' => $host,
            'login' => $login,
            'password' => $password,
            'port' => $port,
            'timeout' => $timeout,
        ];
    }

    /**
     * Возвращает активное подключение к ftp-серверу
     *
     * @return resource|null
     */
    protected function getStream()
    {
        return $this->stream;
    }

    /**
     * Возвращает true, если подключение к ftp-серверу активно
     *
     * @return bool
     */
    public function isOpened()
    {
        return $this->getStream() !== null;
    }

    /**
     * Открывает новое подключение к ftp-серверу
     *
     * @return void
     */
    public function open()
    {
        if ($this->isOpened()) {
            $this->close();
        }

        $this->stream = ftp_connect($this->host, $this->port, $this->timeout);

        ftp_login($this->getStream(), $this->login, $this->password);
        ftp_pasv($this->getStream(), true);
    }

    /**
     * Закрывает активное подключение к ftp-серверу
     *
     * @return void
     */
    public function close()
    {
        if ($this->isOpened()) {
            ftp_quit($this->getStream());

            $this->stream = null;
        }
    }
}
