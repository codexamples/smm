<?php

namespace App\Business\Ftp;

use App\Models\MsgFile;

/**
 * Ftp-клиент у Asterisk Msg File Server
 */
class MsgFileClient extends Client
{
    /**
     * Создает экземпляр, указывая в качестве реквизитов, данные из конфига
     */
    public function __construct()
    {
        parent::__construct(
            config('caller.ftp_asterisk.msg_file.host'),
            config('caller.ftp_asterisk.msg_file.login'),
            config('caller.ftp_asterisk.msg_file.password')
        );
    }

    /**
     * Удаляет файл отбивки с ftp-сервера
     *
     * @param  \App\Models\MsgFile  $file
     * @return void
     */
    public function unlinkFile(MsgFile $file)
    {
        $this->open();

        ftp_delete($this->getStream(), pathinfo($file->getRealPath(), PATHINFO_BASENAME));

        $this->close();
    }

    /**
     * Загружает файл отбивки на ftp-сервер
     *
     * @param  \App\Models\MsgFile  $file
     * @return void
     */
    public function uploadFile(MsgFile $file)
    {
        $this->open();

        $real = $file->getRealPath();
        ftp_put($this->getStream(), pathinfo($real, PATHINFO_BASENAME), $real, FTP_BINARY);

        $this->close();
    }
}
