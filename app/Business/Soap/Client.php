<?php

namespace App\Business\Soap;

use App\Business\UsesCredentials;
use BeSimple\SoapClient\SoapClientBuilder;
use BeSimple\SoapClient\SoapClientOptionsBuilder;
use BeSimple\SoapClient\SoapFaultWithTracingData;
use BeSimple\SoapCommon\SoapOptionsBuilder;

/**
 * Отправка SOAP-запросов
 *
 * @property-read string $wsdl
 */
class Client
{
    use UsesCredentials;

    /**
     * Построитель клиента
     *
     * @var \BeSimple\SoapClient\SoapClientBuilder
     */
    private $clientBuilder;

    /**
     * Клиент
     *
     * @var \BeSimple\SoapClient\SoapClient
     */
    private $client;

    /**
     * Создает экземпляр, прокидывая в него путь к WSDL-файлу
     *
     * @param  string  $wsdl
     */
    public function __construct(string $wsdl)
    {
        $this->credentials = [
            'wsdl' => $wsdl,
        ];

        $this->clientBuilder = new SoapClientBuilder();
        $this->client = $this->clientBuilder->build(
            SoapClientOptionsBuilder::createWithDefaults(),
            SoapOptionsBuilder::createWithDefaults($this->wsdl)
        );
    }

    /**
     * Возвращает построитель клиента
     *
     * @return \BeSimple\SoapClient\SoapClientBuilder
     */
    public function getClientBuilder()
    {
        return $this->clientBuilder;
    }

    /**
     * Возвращает клиента
     *
     * @return \BeSimple\SoapClient\SoapClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Вызывает метод через SOAP, возвращает результат, либо выкинутое исключение
     *
     * @param  string  $method
     * @param  array  $args
     * @return \BeSimple\SoapClient\SoapResponse|\BeSimple\SoapClient\SoapFaultWithTracingData
     */
    public function call(string $method, array $args)
    {
        try {
            return $this->getClient()->soapCall($method, $args);
        } catch (SoapFaultWithTracingData $fault) {
            return $fault;
        }
    }
}
