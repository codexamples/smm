<?php

namespace App\Business\Soap;

use BeSimple\SoapClient\SoapResponse;
use Illuminate\Support\Facades\Log;
use stdClass;

/**
 * SOAP-клиент у SMS-центра
 *
 * @property-read string $login
 * @property-read string $password
 */
class SmsCenterClient extends Client
{
    /**
     * Создает экземпляр, закидывая в родительский ссылку на WSDL-файл
     */
    public function __construct()
    {
        parent::__construct('https://smsc.ru/sys/soap.php?wsdl');

        $this->credentials['login'] = config('caller.sms.center.login');
        $this->credentials['password'] = config('caller.sms.center.password');
    }

    /**
     * Отправляет SMS-сообщение, возвращает true, если сообщение было успешно отправлено
     *
     * @param  string  $sender
     * @param  string  $lead
     * @param  string  $message
     * @return bool
     */
    public function sendSms(string $sender, string $lead, string $message)
    {
        $obj = new stdClass();
        $obj->login = $this->login;
        $obj->psw = $this->password;
        $obj->phones = $lead;
        $obj->mes = $message;
        $obj->sender = $sender;
        $obj->time = 0;

        Log::info('==> Send SMS');
        Log::info('==> From: ' . $sender);
        Log::info('==> To: ' . $lead);
        Log::info('==> Msg: ' . $message);

        $response = $this->call('send_sms', [$obj]);

        if ($response instanceof SoapResponse) {
            Log::info('==> Succeeded');

            return true;
        } else {
            Log::info('==> Failed');

            return false;
        }
    }
}
