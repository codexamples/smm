<?php

namespace App\Business\Api;

use Exception;

/**
 * API-клиент https://maps.googleapis.com/maps/api/geocode/json
 */
class GoogleMapsCoordinates
{
    /**
     * Запрашивает информацию о координатах из Google Maps API
     *
     * @param  string  $line
     * @return string
     */
    public function getCoordinateInfo(string $line)
    {
        $prepend = '';

        if ($this->cutSourceLine($line, $prepend)) {
            return $prepend . ' - ' . $this->parseResponse($this->validateResponse($this->requestData($line)));
        }

        return $line;
    }

    /**
     * Отрезает лишнее от исходной строки, а если нечего отрезать - такая строка не подходит для GM
     *
     * @param  string  &$line
     * @param  string  &$prepend
     * @return bool
     */
    private function cutSourceLine(string &$line, string &$prepend)
    {
        $parts = explode(':', $line);
        $coords = explode(',', $parts[count($parts) - 1]);

        if (count($coords) < 2) {
            return false;
        }

        $prepend = mb_substr($line, 0);
        $line = $coords[0] . ',' . $coords[1];

        return true;
    }

    /**
     * Запрашивает данные о координате из GM
     *
     * @param  string  $line
     * @return mixed
     */
    private function requestData(string $line)
    {
        $ch = curl_init('https://maps.googleapis.com/maps/api/geocode/json?' . http_build_query([
            'latlng' => $line,
            'key' => config('google.maps.token'),
        ]));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        return $error ? new Exception($error) : json_decode($response, true);
    }

    /**
     * Валидирует данные, пришедшие из GM
     *
     * @param  mixed  $response
     * @return array
     * @throws \Exception
     */
    private function validateResponse($response)
    {
        if ($response instanceof \Exception) {
            throw $response;
        }

        if (!is_array($response)) {
            throw new \Exception('Invalid response type');
        }

        if (array_key_exists('error_message', $response)) {
            throw new \Exception($response['error_message']);
        }

        if ($response['status'] !== 'OK') {
            throw new \Exception('Not OK');
        }

        return $response;
    }

    /**
     * Преобразовывает данные от GM, в данные "нашего" формата
     *
     * @param  array  $response
     * @return string
     */
    private function parseResponse(array $response)
    {
        $arr = [];

        foreach ($response['results'][0]['address_components'] as $component) {
            if (in_array('postal_code', $component['types'])) {
                $arr[] = $component['long_name'];
            }

            if (in_array('locality', $component['types'])) {
                $arr[] = $component['long_name'];
            }

            if (in_array('route', $component['types'])) {
                $arr[] = $component['long_name'];
            }

            if (in_array('street_number', $component['types'])) {
                $arr[] = $component['long_name'];
            }
        }

        return implode(',', $arr);
    }
}
