<?php

namespace App\Business\Api;

use Exception;
use Illuminate\Support\Facades\Log;

/**
 * API-клиент Asterisk'a
 */
class Asterisk
{
    /**
     * Совершает звонок, на основе данных с facebook-форм
     *
     * @param  string  $from
     * @param  string  $to
     * @param  string  $msgfilename
     * @param  int  $log_id
     * @param  int  $msg_delay
     * @return void
     */
    public function generateCall(string $from, string $to, string $msgfilename, int $log_id, int $msg_delay)
    {
        $get = $this->prepareGetParams($from, $to);
        Log::info('==> Asterisk GET: ' . $get);

        $post = $this->preparePostParams($msgfilename, $log_id, $msg_delay);
        Log::info('==> Asterisk POST: ' . $post);

        $arr = $this->requestAri($get, $post);

        Log::info('==> Asterisk response: ' . $arr['response']);
        Log::info('==> Asterisk HTTP-code: ' . $arr['info']['http_code']);
        Log::info('==> Asterisk error: ' . $arr['error']);

        $this->validateResponse($arr);
    }

    /**
     * Подготовка GET-параметров для звонка с facebook-форм
     *
     * @param  string  $from
     * @param  string  $to
     * @return string
     */
    private function prepareGetParams(string $from, string $to)
    {
        $get = config('caller.call.get');
        $get['endpoint'] = $get['endpoint']['begin'] . preg_replace('/[^0-9]/', '', $from) . $get['endpoint']['end'];
        $get['extension'] = preg_replace('/[^0-9]/', '', $to);

        return http_build_query($get);
    }

    /**
     * Подготовка POST-параметров для звонка с facebook-форм
     *
     * @param  string  $msgfilename
     * @param  int  $log_id
     * @param  int  $msg_delay
     * @return string
     */
    private function preparePostParams(string $msgfilename, int $log_id, int $msg_delay)
    {
        return json_encode(['variables' => [
            'TRUNK' => config('caller.call.trunk'),
            'msgfilename' => $msgfilename,
            'msg_delay' => strval($msg_delay),
            'ID' => strval($log_id),
        ]]);
    }

    /**
     * Делает запрос в API астера на основе данных с facebook-форм
     *
     * @param  string  $get
     * @param  string  $post
     * @return array
     */
    private function requestAri(string $get, string $post)
    {
        $ch = curl_init('http://185.***.24:***/ari/channels?' . $get);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        $error = curl_error($ch);
        curl_close($ch);

        return [
            'response' => $response,
            'info' => $info,
            'error' => $error,
        ];
    }

    /**
     * Валидирует данные ответа от астера, кидая исключения
     *
     * @param  $arr
     * @return void
     * @throws \Exception
     */
    private function validateResponse($arr)
    {
        if ($arr['error']) {
            throw new Exception($arr['error']);
        }

        if ($arr['info']['http_code'] !== 200) {
            throw new Exception('http-code = ' . $arr['info']['http_code']);
        }

        $json = json_decode($arr['response'], true);

        if ($json) {
            if (array_key_exists('id', $json)) {
                return;
            }

            if (array_key_exists('message', $json)) {
                throw new Exception($json['message']);
            }
        }

        throw new Exception('Неизвестный ответ телефонии');
    }
}
