<?php

namespace App\Business\Mail;

/**
 * Уведомлятор о лидах звонилки по почте
 */
class CallerMailer
{
    use SendsMail;

    /**
     * Уведомляет о новом лиде по почте
     * Возвращает true, если отправка прошла успешно
     *
     * @param  string  $from_mail
     * @param  string  $from_name
     * @param  string  $mails_to
     * @param  string  $event
     * @param  array  $lead_data
     * @return bool
     */
    public function sendLead(string $from_mail, string $from_name, string $mails_to, string $event, array $lead_data)
    {
        $to = explode(',', $mails_to);

        if (empty($to)) {
            return true;
        }

        $lines = [];

        foreach ($lead_data as $key => $value) {
            if ($key === 'token') {
                continue;
            }

            $lines[] = '<b>' . trans('caller.mailgun.' . $key) . ':</b> ' . $value;
        }

        return static::sendMailMessage(
            $from_mail,
            $from_name,
            $to,
            'Caller ' . $event . ' Event',
            implode('<br>', $lines)
        );
    }
}
