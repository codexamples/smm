<?php

namespace App\Business\Mail;

use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Support\Facades\Log;

/**
 * Отправка почты через MailGun
 */
trait SendsMail
{
    /**
     * Отправляет письмо через MailGun, в удобоваримом виде
     * Возвращает true, если письмо было успешно отправлено
     *
     * @param  string  $from_mail
     * @param  string  $from_name
     * @param  array  $to
     * @param  string  $subject
     * @param  string  $text
     * @return bool
     */
    protected static function sendMailMessage(
        string $from_mail,
        string $from_name,
        array $to,
        string $subject,
        string $text
    )
    {
        $to_list = $to;
        $to = [];

        foreach ($to_list as $to_item) {
            $to[$to_item] = [];
        }

        Log::info('==> MailGun params: ' . json_encode([
            'from_mail' => $from_mail,
            'from_name' => $from_name,
            'to' => $to_list,
            'subject' => $subject,
            'text' => $text,
        ]));

        /** @var  \Bogardo\Mailgun\Http\Response  $response */
        $response = Mailgun::raw($text, function ($msg) use ($from_mail, $from_name, $to, $subject) {
            $msg->from($from_mail, $from_name);
            $msg->to($to);
            $msg->subject($subject);
        });

        $success = $response->success();
        Log::info('==> MailGun response: ' . ($success ? 'successful' : 'failed'));

        return $success;
    }
}
