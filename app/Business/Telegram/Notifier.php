<?php

namespace App\Business\Telegram;

use App\Models\CallForm;
use App\Models\TelegramChat;
use App\Models\TelegramLog;
use Exception;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api as LibApi;

/**
 * Оповещатор о лидах в чат-комнаты Telegram
 */
class Notifier
{
    /**
     * Telegram Api Client
     *
     * @var \Telegram\Bot\Api
     */
    private $api;

    /**
     * @param  \Telegram\Bot\Api  $api
     */
    public function __construct(LibApi $api)
    {
        $this->api = $api;
    }

    /**
     * Рассылает лида в подвязанные к формам чат-комнаты Telegram
     *
     * @param  \App\Models\CallForm  $form
     * @param  string  $event
     * @param  array  $lead_data
     * @return void
     */
    public function sendLead(CallForm $form, string $event, array $lead_data)
    {
        $lines = ['!!! ' . $event . ' Lead !!!'];

        foreach ($lead_data as $field => $value) {
            if ($field === 'token') {
                continue;
            }

            $lines[] = trans('caller.mailgun.' . $field) . ': ' . $value;
        }

        $text = implode(PHP_EOL, $lines);
        $log_data = [
            'title' => $lines[0],
            'lead' => $lead_data,
        ];

        foreach ($form->chats as $chat) {
            /** @var  \App\Models\TelegramChat  $chat */

            Log::info('==> Sending lead to telegram...');
            Log::info('==> Chat: ' . $chat->name . ' #' . $chat->id);
            Log::info('==> Data:');
            Log::info($text);

            $log = $this->appendLog($chat, $form, $log_data);

            try {
                $this->api->sendMessage([
                    'chat_id' => $chat->id,
                    'text' => $text,
                ]);

                Log::info('==> Successfully sent');
            } catch (Exception $ex) {
                Log::info('==> Error: ' . $ex->getMessage());

                $log->setError($ex);
            }
        }
    }

    /**
     * Добавляет новую запись в лог по отправкам лидов в Telegram
     *
     * @param  \App\Models\TelegramChat  $chat
     * @param  \App\Models\CallForm  $form
     * @param  array  $lead_data
     * @return \App\Models\TelegramLog
     */
    public function appendLog(TelegramChat $chat, CallForm $form, array $lead_data)
    {
        return TelegramLog::create([
            'chat_id' => $chat->id,
            'telegram_id' => $chat->id,
            'telegram_name' => $chat->name,
            'form_id' => $form->id,
            'center_name' => $form->center->name,
            'form_name' => $form->name,
            'lead_data' => json_encode($lead_data),
            'error' => '',
        ]);
    }
}
