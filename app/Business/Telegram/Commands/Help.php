<?php

namespace App\Business\Telegram\Commands;

use Telegram\Bot\Commands\HelpCommand;

class Help extends HelpCommand
{
    /**
     * @var string The Telegram command description.
     */
    protected $description = 'Список доступных команд';
}
