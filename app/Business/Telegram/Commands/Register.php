<?php

namespace App\Business\Telegram\Commands;

use App\Models\TelegramChat;
use Illuminate\Support\Facades\Log;

class Register extends ChatBasedCommand
{
    /**
     * The name of the Telegram command.
     * Ex: help - Whenever the user sends /help, this would be resolved.
     *
     * @var string
     */
    protected $name = 'register';

    /**
     * @var string The Telegram command description.
     */
    protected $description = 'Регистрирует чат-комнату для возможности оповещать в нее о лидах';

    /**
     * Process the command.
     *
     * @param $arguments
     *
     * @return mixed
     */
    public function handle($arguments)
    {
        $data = $this->getThisChatData();

        Log::info('==> Telegram Register Command');
        Log::info('==> Data: ' . json_encode($data));

        $row = $this->findChat($data);
        $lines = [];

        if ($row) {
            $lines[] = 'Этот чат уже был добавлен ранее.';
            $lines[] = 'Чтобы посмотреть привязанные формы, введите /status';
        } else {
            TelegramChat::create($data);

            $lines[] = 'Этот чат успешно добавлен.';
            $lines[] = '';
            $lines[] = 'Теперь его осталось подвязать к каким-либо формам в DmSmm,';
            $lines[] = 'чтобы получать оповещения о лидах, падающих из этих форм.';
            $lines[] = '';
            $lines[] = 'Чтобы удалить этот чат, введите /unreg';
        }

        return $this->replyWithMessage(['text' => implode(PHP_EOL, $lines)]);
    }
}
