<?php

namespace App\Business\Telegram\Commands;

use App\Models\TelegramChat;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Objects\Chat;

abstract class ChatBasedCommand extends Command
{
    /**
     * Возвращает универсальные данные о чат-комнате Telegram
     *
     * @param  \Telegram\Bot\Objects\Chat  $chat
     * @return array
     */
    protected function getChatData(Chat $chat)
    {
        $name = 'Untitled Chat';

        if ($chat->getTitle()) {
            $name = $chat->getTitle();
        } elseif ($chat->getFirstName() || $chat->getLastName()) {
            $parts = [];

            if ($chat->getFirstName()) {
                $parts[] = $chat->getFirstName();
            }

            if ($chat->getLastName()) {
                $parts[] = $chat->getLastName();
            }

            $name = implode(' ', $parts);
        }

        return [
            'id' => $chat->getId(),
            'name' => $name,
        ];
    }

    /**
     * Возвращает универсальные данные о текущей чат-комнате Telegram
     *
     * @return array
     */
    protected function getThisChatData()
    {
        return $this->getChatData($this->getUpdate()->getMessage()->getChat());
    }

    /**
     * Ищет и возвращает запись в БД о чат-комнате Telegram, по ее универсальным данным
     *
     * @param  array  $data
     * @return \App\Models\TelegramChat|null
     */
    protected function findChat(array $data)
    {
        return TelegramChat::find($data['id']);
    }
}
