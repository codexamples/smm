<?php

namespace App\Business\Telegram\Commands;

use Illuminate\Support\Facades\Log;

class Unreg extends ChatBasedCommand
{
    /**
     * The name of the Telegram command.
     * Ex: help - Whenever the user sends /help, this would be resolved.
     *
     * @var string
     */
    protected $name = 'unreg';

    /**
     * @var string The Telegram command description.
     */
    protected $description = 'Удаляет чат-комнату из списка оповещения о лидах';

    /**
     * Process the command.
     *
     * @param $arguments
     *
     * @return mixed
     */
    public function handle($arguments)
    {
        $data = $this->getThisChatData();

        Log::info('==> Telegram Unreg Command');
        Log::info('==> Data: ' . json_encode($data));

        $row = $this->findChat($data);
        $lines = [];

        if ($row) {
            $row->delete();

            $lines[] = 'Этот чат успешно удален.';
            $lines[] = 'Больше сюда не будут падать оповещения о лидах.';
            $lines[] = '';
            $lines[] = 'Чтобы вернуть возможность получать оповещения о лидах, введите /register';
        } else {
            $lines[] = 'Этот чат уже был удален ранее, либо никогда не добавлялся.';
            $lines[] = 'Чтобы добавить этот чат, введите /register';
        }

        return $this->replyWithMessage(['text' => implode(PHP_EOL, $lines)]);
    }
}
