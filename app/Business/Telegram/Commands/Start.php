<?php

namespace App\Business\Telegram\Commands;

use Illuminate\Support\Facades\Log;
use Telegram\Bot\Commands\Command;

class Start extends Command
{
    /**
     * The name of the Telegram command.
     * Ex: help - Whenever the user sends /help, this would be resolved.
     *
     * @var string
     */
    protected $name = 'start';

    /**
     * @var string The Telegram command description.
     */
    protected $description = 'Приветственный текст';

    /**
     * Process the command.
     *
     * @param $arguments
     *
     * @return mixed
     */
    public function handle($arguments)
    {
        Log::info('==> Telegram Start Command');

        return $this->replyWithMessage(['text' => implode(PHP_EOL, [
            'Здравствуйте!',
            '',
            'Я оповещаю о новых лидах, поступаемых в DmSmm.',
            'Если Вы не знаете, о каком проекте идет речь, либо не имеете доступа к нему',
            'пожалуйста, не тратьте мое и Ваше время.',
            '',
            'Для списка команд, введите /help',
        ])]);
    }
}
