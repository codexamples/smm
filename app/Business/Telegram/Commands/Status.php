<?php

namespace App\Business\Telegram\Commands;

use Illuminate\Support\Facades\Log;

class Status extends ChatBasedCommand
{
    /**
     * The name of the Telegram command.
     * Ex: help - Whenever the user sends /help, this would be resolved.
     *
     * @var string
     */
    protected $name = 'status';

    /**
     * @var string The Telegram command description.
     */
    protected $description = 'Отображает статус чат-комнаты';

    /**
     * Process the command.
     *
     * @param $arguments
     *
     * @return mixed
     */
    public function handle($arguments)
    {
        $data = $this->getThisChatData();

        Log::info('==> Telegram Status Command');
        Log::info('==> Data: ' . json_encode($data));

        $row = $this->findChat($data);
        $lines = [];

        if ($row) {
            if ($row->forms()->count()) {
                $lines[] = 'Формы, подвязанные к этому чату:';
                $n = 0;

                foreach ($row->forms as $form) {
                    $lines[] = (++$n) . '. ' . $form->name . ' (' . $form->facebook_id . ')';
                }
            } else {
                $lines[] = 'Ни одна форма не была подвязана к этому чату.';
            }
        } else {
            $lines[] = 'Этот чат не был добавлен в DmSmm.';
            $lines[] = 'Чтобы добавить, введите /register';
        }

        return $this->replyWithMessage(['text' => implode(PHP_EOL, $lines)]);
    }
}
