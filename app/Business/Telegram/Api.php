<?php

namespace App\Business\Telegram;

use Telegram\Bot\Api as BaseApi;
use Telegram\Bot\Objects\Update;

/**
 * Фиксы некоторых методов Telegram API Client
 */
class Api extends BaseApi
{
    /**
     * Выполняет команды бота, а если команду определить не удалось, вызывает колбек
     *
     * @param  bool  $webhook
     * @param  callable  $unrecognized
     * @return \Telegram\Bot\Objects\Update|\Telegram\Bot\Objects\Update[]
     */
    public function handleCommands(bool $webhook, callable $unrecognized)
    {
        if ($webhook) {
            $this->processCommand($update = $this->fixUpdate($this->getWebhookUpdates()));
        } else {
            $update = $this->commandsHandler($webhook);
        }

        if ($update instanceof Update) {
            $this->recognizeAnyCommand($update, $unrecognized);
        } else {
            foreach ($update as $item) {
                $this->recognizeAnyCommand($item, $unrecognized);
            }
        }

        return $update;
    }

    /**
     * Возвращает обновление с сообщением, если оно запрятано в другом месте JSON-телеграммки
     *
     * @param  \Telegram\Bot\Objects\Update  $update
     * @return \Telegram\Bot\Objects\Update
     */
    protected function fixUpdate(Update $update)
    {
        $data = $update->all();
        $act = 0;

        if (array_key_exists('channel_post', $data)) {
            $data['message'] = $data['channel_post'];
            unset($data['channel_post']);

            $act = 1;
        }

        if ($act && array_key_exists('text', $data['message'])) {
            $parts = explode(' ', trim($data['message']['text']));
            $bot = array_shift($parts);
            $text = $parts ? implode(' ', $parts) : '';

            if ($bot === config('telegram.bot_name') && $text) {
                $data['message']['text'] = $text;
            } else {
                unset($data['message']);
            }
        }

        return $act ? new Update($data) : $update;
    }

    /**
     * Определяет, была ли выполнена какая-либо команда, и если нет, вызывает колбек
     *
     * @param  \Telegram\Bot\Objects\Update  $update
     * @param  callable  $unrecognized
     * @return void
     */
    protected function recognizeAnyCommand(Update $update, callable $unrecognized)
    {
        $msg = $update->getMessage();

        if ($msg === null || !$msg->has('text')) {
            return;
        }

        $matches = $this->getCommandBus()->parseCommand($msg->getText());

        if (empty($matches)) {
            call_user_func($unrecognized, $update);
        }
    }
}
