<?php

namespace App\Business\Caller;

use App\Business\Api\Asterisk;
use App\Business\Mail\CallerMailer;
use App\Business\Soap\SmsCenterClient as SmsCenterSoapClient;
use App\Business\Telegram\Notifier as TelegramNotifier;
use App\Models\CallForm;
use App\Models\CallLog;
use App\Models\CallSchedule;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * Логика звонилки
 */
class Caller
{
    /**
     * API-клиент телефонии
     *
     * @var \App\Business\Api\Asterisk
     */
    private $asterisk;

    /**
     * Уведомлятор по почте о лидах
     *
     * @var \App\Business\Mail\CallerMailer
     */
    private $mailer;

    /**
     * Уведомлятор о лидах через чат-комнаты Telegram
     *
     * @var \App\Business\Telegram\Notifier
     */
    private $telegram;

    /**
     * SMS-рассыльщик
     *
     * @var \App\Business\Soap\SmsCenterClient
     */
    private $sms;

    /**
     * Создает новый экземпляр, закидывая в него основные органы управления звонилкой
     *
     * @param  \App\Business\Api\Asterisk  $asterisk
     * @param  \App\Business\Mail\CallerMailer  $mailer
     * @param  \App\Business\Telegram\Notifier  $telegram
     * @param  \App\Business\Soap\SmsCenterClient  $sms
     */
    public function __construct(
        Asterisk $asterisk,
        CallerMailer $mailer,
        TelegramNotifier $telegram,
        SmsCenterSoapClient $sms
    )
    {
        $this->asterisk = $asterisk;
        $this->mailer = $mailer;
        $this->telegram = $telegram;
        $this->sms = $sms;
    }

    /**
     * Логика создания звонка
     *
     * @param  \App\Models\CallForm  $form
     * @param  array  $args
     * @param  bool  $check_working_time
     * @return void
     * @throws \Exception
     */
    public function generateCall(CallForm $form, array $args, bool $check_working_time)
    {
        /** @var  \App\Models\CallCenter  $center */
        $center = $form->center;

        Log::info('==> Caller::generateCall()');
        Log::info('==> Form: ' . $form->id . ' | ' . $form->facebook_id . ' | ' . $form->name);
        Log::info('==> Center phone: ' . $center->phone);
        Log::info('==> Args: ' . json_encode($args));

        $call_time = array_key_exists('call_time', $args) ? $args['call_time'] : null;
        $wants_call = !array_key_exists('wants_call', $args) || $args['wants_call'] === 'yes';
        $phone_number = array_key_exists('phone_number', $args) ? $args['phone_number'] : '99999999999';

        // THE CALL TIME TIME
        if ($call_time) {
            $call_time = strtotime(date('Y-m-d') . ' ' . $call_time . ':00');

            $args['time_to_call'] = $args['call_time'];
            unset($args['call_time']);

            if ($call_time) {
                $time_invalid = false;
                $call_time = date('Y-m-d H:i', $call_time) . ':00';

                Log::info('==> Call Time: ' . $call_time);
            } else {
                $time_invalid = true;
                $call_time = date('Y-m-d H:i', time() + 60) . ':00';

                Log::info('==> Invalid call time specified');
            }

            $call_time = new Carbon($call_time);
            $now = new Carbon();

            if ($call_time->hour * 60 + $call_time->minute <= $now->hour * 60 + $now->minute) {
                $call_time->addDay();
            }

            $schedule = CallSchedule::findOne($phone_number, $form->id);

            if ($schedule) {
                Log::info('==> Delete scheduling');
                $schedule->delete();
            }

            $schedule = $form
                ->appendSchedule($phone_number, $args, 'custom', 1)
                ->setAt($call_time, false)
                ->correctAt();

            Log::info('==> Corrected to: ' . $schedule->call_back_at);

            $form->sendSmsViaCenter(
                $this->sms,
                $phone_number,
                $time_invalid ? 'time_invalid' : 'time',
                $schedule->getAt()
            );

            $telegram_event = null;

        // ONLY MAILING
        } elseif ($form->mail_only || !$wants_call) {
            Log::info('==> Rejected by ' . ($wants_call ? 'FORM' : 'USER'));

            $form->sendLeadViaMailer($this->mailer, 'REJECT', $args);
            $form->appendLog($phone_number, $args, 'rejected', []);
            $telegram_event = 'Только почта';

        // NOT A WORKING TIME
        } elseif ($check_working_time && !$center->isInWorkingTimeNow()) {
            Log::info('==> Scheduled');

            if (CallSchedule::findOne($phone_number, $form->id)) {
                Log::info('==> Duplication prevention');

                $telegram_event = null;
            } else {
                $schedule = $form->appendSchedule($phone_number, $args, 'schedule', 1)->correctAt();
                $form->sendSmsViaCenter($this->sms, $phone_number, 'schedule', $schedule->getAt());

                $telegram_event = 'Отложен';
            }

        // JUST CALL
        } else {
            $log = $form->appendLog($phone_number, $args, 'talking', []);

            if ($check_working_time) {
                $form->sendSmsViaCenter($this->sms, $phone_number, 'simple', new Carbon());
            }

            try {
                $center->generateCallViaAsterisk($this->asterisk, $phone_number, $log->id);
            } catch (Exception $ex) {
                $log->setStatus('error', ['message' => $ex->getMessage()]);

                throw $ex;
            }

            $telegram_event = 'Начало звонка';
        }

        // FINISH
        if ($telegram_event) {
            $args['event'] = $telegram_event;
            $this->telegram->sendLead($form, 'New', $args);
        }
    }

    /**
     * Логика завершения звонка
     *
     * @param  \App\Models\CallLog  $log
     * @param  int  $state
     * @param  int  $direction
     * @return void
     */
    public function endCall(CallLog $log, int $state, int $direction)
    {
        /** @var  \App\Models\CallForm  $form */
        $form = $log->form;

        /** @var  \App\Models\CallCenter  $center */
        $center = $form->center;

        Log::info('==> Caller::endCall()');
        Log::info('==> Log: ' . $log->id . ' | ' . $log->facebook_data);
        Log::info('==> Form: ' . $form->id . ' | ' . $form->facebook_id . ' | ' . $form->name);
        Log::info('==> Center phone: ' . $center->phone);
        Log::info('==> State: ' . $state . ' | ' . $direction);

        // ALREADY HANGED UP
        if ($log->status_name === 'hangup') {
            Log::info('==> This was already hanged up');

            $data = $log->getStatus()->getData();

            if ($data['state'] === $state) {
                Log::info('==> States are equal');

                $update = false;
                $return = false;
            } else {
                Log::info('==> Update from ' . $data['state'] . ' / ' . $data['direction']);

                $update = true;
                $return = false;
            }

        // NOT HANGED UP YET
        } else {
            Log::info('==> Waiting for the next notification');

            $update = true;
            $return = true;
        }

        // UPDATE TO HANGUP
        if ($update) {
            $log->setStatus('hangup', [
                'state' => $state,
                'direction' => $direction,
            ]);
        }

        // DONT CONTINUE
        if ($return) {
            return;
        }

        // COLLECT DATA
        $status = $log->getStatus();
        $data = $status->getData();
        $state = $data['state'];
        $direction = $data['direction'];

        $unavailable = in_array($state, [0, 17, 19]);
        $unavailable_center = $unavailable && $direction < 0;
        $unavailable_lead = $unavailable && $direction > 0;
        $conversed = $state === 16;
        $again = null;

        // UNAVAILABLE CENTER
        if ($unavailable_center) {
            Log::info('==> Unavailable CENTER');

            $again = [
                'type' => 'unavailable_center',
                'count' => 1,
                'interval' => config('caller.working_time.call_back'),
                'schedule' => CallSchedule::findOne($log->lead_phone, $form->id),
                'alt_count' => $center->getLeadCallBackTimes(),
                'alt_interval' => $center->getLeadCallBackInterval(),
            ];
        }

        // UNAVAILABLE LEAD
        if ($unavailable_lead) {
            Log::info('==> Unavailable LEAD');

            $times = $center->getLeadCallBackTimes();
            $interval = $center->getLeadCallBackInterval();

            if ($times && $interval) {
                $again = [
                    'type' => 'unavailable_lead',
                    'count' => $times,
                    'interval' => $interval,
                    'schedule' => CallSchedule::findOne($log->lead_phone, $form->id),
                    'alt_count' => $times,
                    'alt_interval' => $interval,
                ];
            } else {
                Log::info('==> No call backs to set');
            }
        }

        // LEAD HAS BEEN COUNTED
        if ($unavailable_center || $conversed) {
            $fb = $log->getFacebookData();
            $fb['event'] = $status->getViewText();

            $form->sendLeadViaMailer($this->mailer, 'HANGUP', $fb);
            $this->telegram->sendLead($form, 'Hangup', $fb);
        }

        // SCHEDULE AGAIN
        if ($again) {
            /** @var  \App\Models\CallSchedule  $schedule */
            $schedule = $again['schedule'];

            $type = $again['type'];
            $count = $again['alt_count'] ? $again['alt_count'] : $again['count'];
            $interval = $again['alt_interval'] ? $again['alt_interval'] : $again['interval'];

            if ($schedule) {
                if ($schedule->call_back_count === 0) {
                    Log::info('==> No call backs left');

                    $schedule->delete();
                    $deleted = true;
                } else {
                    Log::info('==> ' . $schedule->call_back_count . ' call backs remaining');
                    $deleted = false;
                }
            } else {
                Log::info('==> New schedule ' . $type . ' with ' . $count . ' call backs');

                $schedule = $log->appendSchedule($type, $count);
                $deleted = false;
            }

            if (!$deleted) {
                $schedule
                    ->setAt(new Carbon(), false)
                    ->addAtMinutes($interval, true)
                    ->correctAt();

                Log::info('==> Every ' . $interval . ' minutes');
            }

        // DROP SCHEDULE
        } else {
            $schedule = CallSchedule::findOne($log->lead_phone, $form->id);

            if ($schedule) {
                Log::info('==> Delete scheduling');
                $schedule->delete();
            }
        }
    }

    /**
     * Логика перезвона
     * Возвращает true, если логику необходимо прервать до следующего раза
     *
     * @param  \App\Models\CallSchedule  $schedule
     * @return bool
     */
    public function callBack(CallSchedule $schedule)
    {
        /** @var  \App\Models\CallForm  $form */
        $form = $schedule->form;

        /** @var  \App\Models\CallCenter  $center */
        $center = $form->center;

        Log::info('==> Caller::callBack()');
        Log::info('==> Schedule: ' . $schedule->type);
        Log::info('==> Call back: ' . $schedule->call_back_count . ' / ' . $schedule->call_back_at);
        Log::info('==> Data: ' . $schedule->facebook_data);
        Log::info('==> Form: ' . $form->id . ' | ' . $form->facebook_id . ' | ' . $form->name);
        Log::info('==> Center phone: ' . $center->phone);

        // IS A WORKING TIME
        if ($schedule->isCenterInWorkingTimeNow($center)) {
            Log::info('==> It\'s a working time now');

            if ($schedule->call_back_count > 0) {
                if ($schedule->type === 'unavailable_lead') {
                    $schedule->addAtMinutes(1024, false);
                } else {
                    $schedule->addAtMinutes(0, true);
                }
            }

            $data = $schedule->getFacebookData();

            if ($schedule->call_back_count === 0 && $schedule->type !== 'unavailable_lead') {
                Log::info('==> Delete scheduling');
                $schedule->delete();
            }

            Log::info('==> Call the lead');

            $this->generateCall($form, $data, false);
            return true;

        // NOT A WORKING TIME
        } else {
            Log::info('==> Not a working time');
            Log::info('==> Correct and skip');

            $schedule->correctAt();
            return false;
        }
    }

    /**
     * Логика сохранения записи разговора
     *
     * @param  \App\Models\CallLog  $log
     * @param  string  $name
     * @param  int  $duration
     * @return void
     */
    public function saveRecordSound(CallLog $log, string $name, int $duration)
    {
        /** @var  \App\Models\CallForm  $form */
        $form = $log->form;

        /** @var  \App\Models\CallCenter  $center */
        $center = $form->center;

        Log::info('==> Caller::saveRecordSound()');
        Log::info('==> Log: ' . $log->id . ' | ' . $log->facebook_data);
        Log::info('==> Form: ' . $form->id . ' | ' . $form->facebook_id . ' | ' . $form->name);
        Log::info('==> Center phone: ' . $center->phone);
        Log::info('==> File name / Duration: ' . $name . ' / ' . $duration);

        $path = 'call_records/' . date('Y') . '/' . date('m') . '/' . date('d') . '/' . $name;

        if (file_exists(storage_path($path))) {
            $log->record_path = $path;

            Log::info('==> Saved');
        } else {
            Log::info('==> File does not exist');
        }

        $log->record_duration = $duration;
        $log->save();
    }
}
