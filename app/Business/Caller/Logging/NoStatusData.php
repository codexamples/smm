<?php

namespace App\Business\Caller\Logging;

/**
 * Представляет статус записи лога звонилки, который не подразумевает каких-то дополнительных данных
 */
trait NoStatusData
{
    /**
     * Возвращает структурированные данные статуса
     *
     * @return array|null
     */
    public function getData()
    {
        return null;
    }
}
