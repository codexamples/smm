<?php

namespace App\Business\Caller\Logging;

/**
 * Представляет статус записи лога звонилки
 */
interface Status
{
    /**
     * Возвращает структурированные данные статуса
     *
     * @return array|null
     */
    public function getData();

    /**
     * Возвращает стили для отображения статуса в журнал
     *
     * @return array
     */
    public function getViewStyle();

    /**
     * Возвращает текст для отображения статуса в журнал
     *
     * @return string
     */
    public function getViewText();
}
