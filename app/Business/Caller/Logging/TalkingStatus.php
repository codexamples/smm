<?php

namespace App\Business\Caller\Logging;

/**
 * Статус записи лога звонилки "разговаривают"
 */
class TalkingStatus implements Status
{
    use NoStatusData;

    /**
     * Возвращает стили для отображения статуса в журнал
     *
     * @return array
     */
    public function getViewStyle()
    {
        return [
            'background-color' => '#ccc',
            'color' => '#777 !important',
        ];
    }

    /**
     * Возвращает текст для отображения статуса в журнал
     *
     * @return string
     */
    public function getViewText()
    {
        return 'Разговаривают';
    }
}
