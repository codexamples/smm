<?php

namespace App\Business\Caller\Logging;

/**
 * Статус записи лога звонилки "ошибка"
 */
class ErrorStatus implements Status
{
    /**
     * Текст ошибки
     *
     * @var string
     */
    private $message;

    /**
     * Создает новый экземпляр, определяя детали ошибки
     *
     * @param  string  $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Возвращает структурированные данные статуса
     *
     * @return array|null
     */
    public function getData()
    {
        return [
            'message' => $this->message,
        ];
    }

    /**
     * Возвращает стили для отображения статуса в журнал
     *
     * @return array
     */
    public function getViewStyle()
    {
        return [
            'background-color' => '#f88',
            'color' => '#200 !important',
        ];
    }

    /**
     * Возвращает текст для отображения статуса в журнал
     *
     * @return string
     */
    public function getViewText()
    {
        return $this->message;
    }
}
