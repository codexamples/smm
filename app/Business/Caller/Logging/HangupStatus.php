<?php

namespace App\Business\Caller\Logging;

/**
 * Статус записи лога звонилки "завершен"
 */
class HangupStatus implements Status
{
    /**
     * Каким образом был завершен звонок
     *
     * @var int
     */
    private $state;

    /**
     * С чьей стороны был завершен звонок
     *
     * @var int
     */
    private $direction;

    /**
     * Стиль отображения в журнал
     *
     * @var array
     */
    private $style;

    /**
     * Текст отображения в журнал
     *
     * @var string
     */
    private $text;

    /**
     * Создает новый экземпляр, определяя детали завершения звонка
     *
     * @param  int  $state
     * @param  int  $direction
     */
    public function __construct(int $state, int $direction)
    {
        $this->state = $state;
        $this->direction = $direction;
    }

    /**
     * Возвращает структурированные данные статуса
     *
     * @return array|null
     */
    public function getData()
    {
        return [
            'state' => $this->state,
            'direction' => $this->direction,
        ];
    }

    /**
     * Возвращает стили для отображения статуса в журнал
     *
     * @return array
     */
    public function getViewStyle()
    {
        $this->prepare();

        return $this->style;
    }

    /**
     * Возвращает текст для отображения статуса в журнал
     *
     * @return string
     */
    public function getViewText()
    {
        $this->prepare();

        return $this->text;
    }

    /**
     * Подготавливает данные для вывода в журнал
     *
     * @return void
     */
    private function prepare()
    {
        if ($this->style !== null && $this->text !== null) {
            return;
        }

        switch ($this->state) {
            case 16: // NORMAL
                $this->prepareSuccessful();
                break;

            case 0: // NOTDEFINED
            case 17: // BUSY
            case 19: // NO_ANSWER
                $this->prepareBusy();
                break;

            case 34: // CONGESTION
            case 38: // FAILURE
                $this->prepareFailure();
                break;

            default:
                $this->prepareUnknown();
                break;
        }

        if ($this->direction < 0) {
            $this->text .= ' - Колцентр';
        } elseif ($this->direction > 0) {
            $this->text .= ' - Лид';
        }
    }

    /**
     * Подготавливает данные в случае успеха
     *
     * @return void
     */
    private function prepareSuccessful()
    {
        $this->style = [
            'background-color' => '#080',
            'color' => '#eff !important',
        ];

        $this->text = 'Звонок завершен';
    }

    /**
     * Подготавливает данные в случае занятости
     *
     * @return void
     */
    private function prepareBusy()
    {
        $this->style = [
            'background-color' => '#c82',
            'color' => '#eff !important',
        ];

        switch ($this->state) {
            case 0:
                $this->text = 'Абонент недоступен';
                break;

            case 17:
                $this->text = 'Занято';
                break;

            case 19:
                $this->text = 'Нет ответа';
                break;
        }
    }

    /**
     * Подготавливает данные в случае ошибки
     *
     * @return void
     */
    private function prepareFailure()
    {
        $this->style = [
            'background-color' => '#c02',
            'color' => '#eff !important',
        ];

        switch ($this->state) {
            case 34:
                $this->text = 'Телефония перегружена';
                break;

            case 38:
                $this->text = 'Ошибка телефонии';
                break;
        }
    }

    /**
     * Подготавливает данные в случае неизвестности
     *
     * @return void
     */
    private function prepareUnknown()
    {
        $this->style = [
            'background-color' => '#840',
            'color' => '#eff !important',
        ];

        $this->text = 'Код "' . $this->state . '"';
    }
}
