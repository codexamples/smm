<?php

namespace App\Business\Caller\Logging;

/**
 * Неизвестный статус записи лога звонилки
 */
class UnknownStatus implements Status
{
    /**
     * Название неизвестного статуса
     *
     * @var string
     */
    private $status;

    /**
     * Данные о неизвестном статусе
     *
     * @var array
     */
    private $args;

    /**
     * Создает новый экземпляр, определяя детали неизвестности
     *
     * @param  string  $status
     * @param  array  $args
     */
    public function __construct(string $status, array $args)
    {
        $this->status = $status;
        $this->args = $args;
    }

    /**
     * Возвращает структурированные данные статуса
     *
     * @return array|null
     */
    public function getData()
    {
        return $this->args;
    }

    /**
     * Возвращает стили для отображения статуса в журнал
     *
     * @return array
     */
    public function getViewStyle()
    {
        return [
            'background-color' => '#444',
            'color' => '#eee !important',
        ];
    }

    /**
     * Возвращает текст для отображения статуса в журнал
     *
     * @return string
     */
    public function getViewText()
    {
        return '"' . $this->status . '"';
    }
}
