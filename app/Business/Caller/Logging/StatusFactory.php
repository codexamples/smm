<?php

namespace App\Business\Caller\Logging;

/**
 * Фабрика статусов записей лога звонилки
 */
class StatusFactory
{
    /**
     * Реестр ранее созданных экземпляров статусов
     *
     * @var array
     */
    private $registry = [];

    /**
     * Создает экземпляр статуса, по его данным
     *
     * @param  string  $status
     * @param  array  $args
     * @return \App\Business\Caller\Logging\Status
     */
    public function makeStatus(string $status, array $args)
    {
        $hash = $this->calcStatusHash($status, $args);

        if (!array_key_exists($hash, $this->registry)) {
            $this->registry[$hash] = $this->createStatus($status, $args);
        }

        return $this->registry[$hash];
    }

    /**
     * Хеширует данные статуса для реестра
     *
     * @param  string  $status
     * @param  array  $args
     * @return string
     */
    private function calcStatusHash(string $status, array $args)
    {
        return md5(json_encode([
            'status' => $status,
            'args' => $args,
        ]));
    }

    /**
     * Создает новый экземпляр статуса
     *
     * @param  string  $status
     * @param  array  $args
     * @return \App\Business\Caller\Logging\Status
     */
    private function createStatus(string $status, array $args)
    {
        switch ($status) {
            case 'talking':
                return new TalkingStatus();

            case 'hangup':
                return new HangupStatus($args['state'], $args['direction']);

            case 'rejected':
                return new RejectedStatus();

            case 'error':
                return new ErrorStatus($args['message']);

            default:
                return new UnknownStatus($status, $args);
        }
    }
}
