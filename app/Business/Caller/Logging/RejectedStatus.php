<?php

namespace App\Business\Caller\Logging;

/**
 * Статус записи лога звонилки "не состоялся"
 */
class RejectedStatus implements Status
{
    use NoStatusData;

    /**
     * Возвращает стили для отображения статуса в журнал
     *
     * @return array
     */
    public function getViewStyle()
    {
        return [
            'background-color' => '#400',
            'color' => '#ffc !important',
        ];
    }

    /**
     * Возвращает текст для отображения статуса в журнал
     *
     * @return string
     */
    public function getViewText()
    {
        return 'Не состоялся';
    }
}
