<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Support\Facades\Log;

abstract class Command extends BaseCommand
{
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    abstract public function handle();

    /**
     * Write a string as information output.
     *
     * @param  string  $string
     * @param $verbosity null|int|string
     *
     * @return void
     */
    public function info($string, $verbosity = null)
    {
        Log::info($string);
        parent::info($string, $verbosity);
    }
}
