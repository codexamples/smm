<?php

namespace App\Console\Commands;

use App\Business\Caller\Caller;
use App\Models\CallLog;
use App\Models\CallSchedule;

class CallBack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'call:back';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calls back busy/scheduled leads';

    /**
     * Логика звонилки
     *
     * @var \App\Business\Caller\Caller
     */
    private $caller;

    /**
     * Create a new console command instance.
     *
     * @param  \App\Business\Caller\Caller  $caller
     */
    public function __construct(Caller $caller)
    {
        parent::__construct();

        $this->caller = $caller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('==> Call:Back start');

        /** @var  \Illuminate\Database\Eloquent\Builder  $q */
        $q = CallSchedule::where('call_back_at', '<=', date('Y-m-d H:i:s'))->orderBy('call_back_at', 'asc');
        $index = 1;
        $count = $q->count();

        $this->info('==> Found count: ' . $count);

        foreach ($q->get() as $schedule) {
            /** @var  \App\Models\CallSchedule  $schedule */
            $this->info('==> Call:Back step: ' . $index . ' / ' . $count);

            if ($this->caller->callBack($schedule)) {
                break;
            }

            $index++;
        }

        $this->info('==> Call:Back ok');
        return 0;
    }
}
