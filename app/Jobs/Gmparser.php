<?php

namespace App\Jobs;

use App\Business\Api\GoogleMapsCoordinates;
use App\Models\Gmparse;
use Exception;

class Gmparser extends Job
{
    /**
     * Создает экземпляр фоновой задачи, прокидывая id модели, которую необходимо отработать
     *
     * @param  \App\Business\Api\GoogleMapsCoordinates  $gm_api
     * @param $model_id
     */
    public function __construct(GoogleMapsCoordinates $gm_api, $model_id)
    {
        $this->gm_api = $gm_api;
        $this->model_id = $model_id;
    }

    /**
     * Экземпляр API-клиента google maps
     *
     * @var \App\Business\Api\GoogleMapsCoordinates
     */
    public $gm_api;

    /**
     * ID модели Gmparse, которую необходимо отработать
     *
     * @var int
     */
    public $model_id;

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $gmparse = Gmparse::findOrFail($this->model_id);
        $lines = explode("\n", $gmparse->lines_in);

        /*
         * FAST STATUSES
         */

        if ($gmparse->status === 'ready') {
            return;
        }

        if ($gmparse->status === '') {
            $json = [
                'type' => 'parsing',
                'parsed' => 0,
                'all' => count($lines),
            ];

            $this->updateStatus($gmparse, $json);
        } else {
            $json = json_decode($gmparse->status, true);
        }

        /*
         * PREPARE
         */

        if (!is_array($json)) {
            throw new \Exception('Invalid status "' . $gmparse->status . '"');
        }

        switch ($json['type']) {
            case 'error':
                $json['type'] = 'parsing';
                unset($json['msg']);

                $this->updateStatus($gmparse, $json);
                break;

            case 'parsing':
                break;

            default:
                throw new \Exception('Invalid status type: ' . $json['type']);
        }

        if (!array_key_exists('parsed', $json) || !array_key_exists('all', $json)) {
            $json['parsed'] = 0;
            $json['all'] = count($lines);

            $gmparse->lines_out = '';
            $this->updateStatus($gmparse, $json);
        }

        /*
         * GO
         */

        while (true) {
            $a = 1 * $json['parsed'];
            $b = 1 * $json['all'];

            if ($a >= $b) {
                $this->updateStatus($gmparse, 'ready');
                break;
            }

            $line = $this->gm_api->getCoordinateInfo($lines[$a]);

            if ($a > 0) {
                $gmparse->lines_out .= "\n";
            }

            $gmparse->lines_out .= $line;
            $json['parsed'] = $a + 1;

            $this->updateStatus($gmparse, $json);
            usleep(50000);
        }
    }

    /**
     * The job failed to process.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $gmparse = Gmparse::find($this->model_id);

        if ($gmparse) {
            $json = json_decode($gmparse->status, true);

            if (is_array($json)) {
                $json['type'] = 'error';
                $json['msg'] = $exception->getMessage();
            } else {
                $json = [
                    'type' => 'error',
                    'msg' => $exception->getMessage(),
                ];
            }

            $this->updateStatus($gmparse, $json);
        }
    }

    /**
     * Обновляет статус модели Gmparse
     *
     * @param  \App\Models\Gmparse  $gmparse
     * @param  $status
     * @return void
     */
    private function updateStatus(Gmparse $gmparse, $status)
    {
        $gmparse->status = is_array($status) ? json_encode($status) : strval($status);
        $gmparse->save();
    }
}
