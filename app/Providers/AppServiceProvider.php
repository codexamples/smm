<?php

namespace App\Providers;

use App\Business\Api\Asterisk;
use App\Business\Api\GoogleMapsCoordinates;
use App\Business\Caller\Caller;
use App\Business\Caller\Logging\StatusFactory;
use App\Business\Ftp\MsgFileClient as MsgFileFtpClient;
use App\Business\Mail\CallerMailer;
use App\Business\Telegram\Notifier as TelegramNotifier;
use Http\Adapter\Guzzle6\Client;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('mailgun.client', function () {
            return new Client();
        });

        $this->app->singleton(GoogleMapsCoordinates::class);
        
        $this->app->singleton(StatusFactory::class);
        $this->app->singleton(Asterisk::class);
        $this->app->singleton(CallerMailer::class);
        $this->app->singleton(TelegramNotifier::class);
        $this->app->singleton(Caller::class);

        $this->app->singleton(MsgFileFtpClient::class);
    }
}
