<?php

namespace App\Models;

use App\Business\Api\Asterisk;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CallCenter extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'works_from',
        'works_to',
        'works_weekends',
        'client_mails',
        'works_weekends_from',
        'works_weekends_to',
        'calls_back',
        'calls_back_times',
        'calls_back_interval',
        'msg_delay',
        'msg_file_hash',
    ];

    /**
     * Center:Form - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forms()
    {
        return $this->hasMany(CallForm::class, 'center_id');
    }

    /**
     * MsgFile:CallCenter - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function msgFile()
    {
        return $this->belongsTo(MsgFile::class, 'msg_file_hash');
    }

    /**
     * Возвращает true, если текущее время - рабочее
     *
     * @param  \Carbon\Carbon|null  $now
     * @return bool
     */
    public function isInWorkingTimeNow($now = null)
    {
        if (!($now instanceof Carbon)) {
            $now = new Carbon();
        }

        $after_midnight = $now->hour * 3600 + $now->minute * 60 + $now->second;
        $is_weekend_now = in_array($now->dayOfWeek, [Carbon::SATURDAY, Carbon::SUNDAY]);

        $from = null;
        $to = null;

        $get_time = function ($s) {
            return strtotime($s) % 86400 + 10800;
        };

        if ($is_weekend_now && $this->works_weekends) {
            if ($this->works_weekends_from && $this->works_weekends_to) {
                $from = call_user_func_array($get_time, [$this->works_weekends_from]);
                $to = call_user_func_array($get_time, [$this->works_weekends_to]);
            }
        } elseif ($is_weekend_now) {
            return false;
        } elseif ($this->works_from && $this->works_to) {
            $from = call_user_func_array($get_time, [$this->works_from]);
            $to = call_user_func_array($get_time, [$this->works_to]);
        }

        if (!$from || !$to) {
            $from = call_user_func_array($get_time, [config('caller.working_time.from')]);
            $to = call_user_func_array($get_time, [config('caller.working_time.to')]);
        }

        return $after_midnight >= $from && $after_midnight <= $to;
    }

    /**
     * Генерирует звонок через телефонию
     *
     * @param  \App\Business\Api\Asterisk  $asterisk
     * @param  string  $lead_phone
     * @param  int  $log_id
     * @return static
     */
    public function generateCallViaAsterisk(Asterisk $asterisk, string $lead_phone, int $log_id)
    {
        $asterisk->generateCall(
            $this->phone,
            $lead_phone,
            $this->msg_file_hash ? $this->msg_file_hash : config('caller.call.msgfilename'),
            $log_id,
            $this->msg_delay
        );

        return $this;
    }

    /**
     * Возвращает количество перезвонов, нужных для лида, если по его вине не состоялся звонок
     *
     * @return int|null
     */
    public function getLeadCallBackTimes()
    {
        if ($this->calls_back) {
            return $this->calls_back_times ? $this->calls_back_times : config('caller.calls_back.times');
        }

        return null;
    }

    /**
     * Возвращает интервал перезвонов, нужных для лида, если по его вине не состоялся звонок
     *
     * @return int|null
     */
    public function getLeadCallBackInterval()
    {
        if ($this->calls_back) {
            return $this->calls_back_interval ? $this->calls_back_interval : config('caller.calls_back.interval');
        }

        return null;
    }
}
