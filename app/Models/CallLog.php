<?php

namespace App\Models;

use App\Business\Caller\Logging\StatusFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CallLog extends Model
{
    use BelongsToCallForm, AccessesFacebookData;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'form_id',
        'center_name',
        'form_name',
        'lead_phone',
        'facebook_data',
        'status_name',
        'status_data',
        'frozen_at',
        'record_path',
        'record_duration',
        'center_phone',
    ];

    /**
     * Возвращает данные о статусе в структурном виде
     *
     * @return \App\Business\Caller\Logging\Status
     */
    public function getStatus()
    {
        /** @var  \App\Business\Caller\Logging\StatusFactory  $factory */
        $factory = app()->make(StatusFactory::class);

        $data = json_decode($this->status_data, true);
        if (!$data) {
            $data = [];
        }

        return $factory->makeStatus($this->status_name, $data);
    }

    /**
     * Задает новые данные о статусе
     *
     * @param  string  $status
     * @param  array  $args
     * @return static
     */
    public function setStatus(string $status, array $args)
    {
        $this->status_name = $status;
        $this->status_data = json_encode($args);
        $this->frozen_at = new Carbon();
        $this->save();

        return $this;
    }

    /**
     * Создает запись в расписании перезвонов, на основе текущей записи лога
     *
     * @param  string  $type
     * @param  int  $call_back_count
     * @return \App\Models\CallSchedule
     */
    public function appendSchedule(string $type, int $call_back_count)
    {
        $at = new Carbon();
        $at->second = 0;

        return CallSchedule::create([
            'hash' => md5(microtime()),
            'lead_phone' => $this->lead_phone,
            'form_id' => $this->form_id,
            'facebook_data' => $this->facebook_data,
            'type' => $type,
            'call_back_count' => $call_back_count,
            'call_back_at' => $at,
        ]);
    }

    /**
     * Производит поиск и фильтрацию логов, возвращая найденные записи
     *
     * @param  $args
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function search($args)
    {
        $q = static::query();

        if (is_array($args)) {
            if (array_key_exists('date_from', $args) && array_key_exists('date_to', $args)) {
                $from = strtotime($args['date_from']);
                $to = strtotime($args['date_to']);

                if ($from !== false && $to !== false && $from <= $to) {
                    $q->whereBetween('frozen_at', [date('Y-m-d 00:00:00', $from), date('Y-m-d 23:59:59', $to)]);
                }
            }

            if (array_key_exists('form', $args) && is_array($args['form']) && count($args['form'])) {
                $q->whereIn('form_id', $args['form']);
            }

            if (array_key_exists('lead_phone', $args) && mb_strlen($args['lead_phone'])) {
                $q->where('lead_phone', 'like', '%' . $args['lead_phone'] . '%');
            }

            if (array_key_exists('facebook_data', $args) && mb_strlen($args['facebook_data'])) {
                $q->where('facebook_data', 'like', '%' . $args['facebook_data'] . '%');
            }
        }

        return $q->orderBy('frozen_at', 'desc')->paginate(25);
    }
}
