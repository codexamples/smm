<?php

namespace App\Models;

use App\Business\Mail\CallerMailer;
use App\Business\Soap\SmsCenterClient as SmsCenterSoapClient;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CallForm extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'center_id',
        'facebook_id',
        'name',
        'mail_only',
        'from_mail',
        'from_name',
        'chat_id',
        'sms_sender',
        'sms_text_simple',
        'sms_text_schedule',
        'sms_text_time',
        'sms_text_time_invalid',
    ];

    /**
     * Center:Form - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function center()
    {
        return $this->belongsTo(CallCenter::class, 'center_id');
    }

    /**
     * Form:Log - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(CallLog::class, 'form_id');
    }

    /**
     * TelegramChat:CallForm - m:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chats()
    {
        return $this->belongsToMany(TelegramChat::class, 'call_form_telegram_chat', 'form_id', 'chat_id');
    }

    /**
     * Шлет лида по почте через специальный мейлер
     *
     * @param  \App\Business\Mail\CallerMailer  $mailer
     * @param  string  $event
     * @param  array  $lead_data
     * @return bool
     */
    public function sendLeadViaMailer(CallerMailer $mailer, string $event, array $lead_data)
    {
        return $mailer->sendLead(
            $this->from_mail ? $this->from_mail : config('mailgun.from.address'),
            $this->from_name ? $this->from_name : config('mailgun.from.name'),
            $this->center->client_mails,
            $event,
            $lead_data
        );
    }

    /**
     * Создает новую запись лога звонилки, которая будет относиться к текущей форме
     *
     * @param  string  $lead_phone
     * @param  array  $lead_data
     * @param  string  $status_name
     * @param  array  $status_data
     * @return \App\Models\CallLog
     */
    public function appendLog(string $lead_phone, array $lead_data, string $status_name, array $status_data)
    {
        /** @var  \App\Models\CallLog  $log */
        $log = CallLog::create([
            'form_id' => $this->id,
            'center_name' => $this->center->name,
            'form_name' => $this->name,
            'lead_phone' => $lead_phone,
            'facebook_data' => '',
            'status_name' => 'temp',
            'status_data' => '',
            'frozen_at' => new Carbon(),
            'center_phone' => $this->center->phone,
        ]);

        return $log
            ->setFacebookData($lead_data)
            ->setStatus($status_name, $status_data);
    }

    /**
     * Создает новую запись в расписании перезвонов, которая будет относиться к текущей форме
     *
     * @param  string  $lead_phone
     * @param  array  $lead_data
     * @param  string  $type
     * @param  int  $call_back_count
     * @return \App\Models\CallSchedule
     */
    public function appendSchedule(string $lead_phone, array $lead_data, string $type, int $call_back_count)
    {
        $at = new Carbon();
        $at->second = 0;

        /** @var  \App\Models\CallSchedule  $schedule */
        $schedule = CallSchedule::create([
            'hash' => md5(microtime()),
            'lead_phone' => $lead_phone,
            'form_id' => $this->id,
            'facebook_data' => '',
            'type' => $type,
            'call_back_count' => $call_back_count,
            'call_back_at' => $at,
        ]);

        return $schedule->setFacebookData($lead_data);
    }

    /**
     * Возвращает готовый текст для SMS из шаблона
     *
     * @param  string  $template
     * @param  \Carbon\Carbon  $now
     * @return string
     */
    public static function getSmsText(string $template, Carbon $now)
    {
        $dynamical = false;
        $result = [];

        foreach (explode('%', $template) as $part) {
            if ($dynamical) {
                $result[] = $part === 'n' ? PHP_EOL : date($part, $now->getTimestamp());
            } else {
                $result[] = $part;
            }

            $dynamical = !$dynamical;
        }

        return implode('', $result);
    }

    /**
     * Возвращает готовый текст для SMS из шаблона, что берется с самой модели
     *
     * @param  string  $field
     * @param  \Carbon\Carbon  $now
     * @return string
     */
    public function getThisSmsText(string $field, Carbon $now)
    {
        $full_field = 'sms_text_' . $field;
        $text = $this->$full_field;

        return static::getSmsText($text ? $text : config('caller.sms.texts.' . $field), $now);
    }

    /**
     * Отправляет SMS-сообщение из-под формы, беря его поля
     *
     * @param  \App\Business\Soap\SmsCenterClient  $sms
     * @param  string  $lead
     * @param  string  $field
     * @param  \Carbon\Carbon  $now
     * @return static
     */
    public function sendSmsViaCenter(SmsCenterSoapClient $sms, string $lead, string $field, Carbon $now)
    {
        $sms->sendSms(
            $this->sms_sender ? $this->sms_sender : config('caller.sms.sender'),
            preg_replace('/[^0-9]/', '', $lead),
            $this->getThisSmsText($field, $now)
        );

        return $this;
    }
}
