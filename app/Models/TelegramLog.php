<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

class TelegramLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'chat_id',
        'telegram_id',
        'telegram_name',
        'form_id',
        'center_name',
        'form_name',
        'lead_data',
        'error',
    ];

    /**
     * Chat:Log - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo(TelegramChat::class, 'chat_id');
    }

    /**
     * CallForm:TelegramChat - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(CallForm::class, 'form_id');
    }

    /**
     * Задает ошибку через исключение
     *
     * @param  \Exception  $ex
     * @return void
     */
    public function setError(Exception $ex)
    {
        $this->error = $ex->getMessage();
        $this->save();
    }

    /**
     * Возвращает данные лога, связанные с лидом
     *
     * @return array|null
     */
    public function getLeadData()
    {
        return $this->lead_data ? json_decode($this->lead_data, true) : null;
    }
}
