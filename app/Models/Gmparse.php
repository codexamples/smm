<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gmparse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lines_in',
        'lines_out',
        'status',
    ];
}
