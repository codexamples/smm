<?php

namespace App\Models;

trait BelongsToCallForm
{
    /**
     * Form:Log - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function form()
    {
        return $this->belongsTo(CallForm::class, 'form_id');
    }
}
