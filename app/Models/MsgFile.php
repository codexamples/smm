<?php

namespace App\Models;

use App\Business\Ftp\MsgFileClient as MsgFileFtpClient;
use Illuminate\Database\Eloquent\Model;

class MsgFile extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'hash';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hash',
        'name',
        'path',
    ];

    /**
     * MsgFile:CallCenter - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function centers()
    {
        return $this->hasMany(CallCenter::class, 'msg_file_hash');
    }

    /**
     * Возвращает реальный путь к файлу отбивки
     *
     * @return string
     */
    public function getRealPath()
    {
        return storage_path($this->path);
    }

    /**
     * Удаляет физический файл отбивки
     *
     * @param  \App\Business\Ftp\MsgFileClient  $ftp
     * @return static
     */
    public function unlinkReal(MsgFileFtpClient $ftp)
    {
        $real = $this->getRealPath();

        if (file_exists($real)) {
            unlink($real);
            $ftp->unlinkFile($this);
        }

        return $this;
    }

    /**
     * Загружает физический файл отбивки
     *
     * @param  \Illuminate\Http\UploadedFile|null  $uploaded
     * @param  \App\Business\Ftp\MsgFileClient  $ftp
     * @return static
     */
    public function uploadReal($uploaded, MsgFileFtpClient $ftp)
    {
        if ($uploaded) {
            $this->unlinkReal($ftp);

            $real = $this->getRealPath();
            $path = pathinfo($real, PATHINFO_DIRNAME);
            $name = pathinfo($real, PATHINFO_BASENAME);

            $uploaded->move($path, $name);
            $ftp->uploadFile($this);
        }

        return $this;
    }
}
