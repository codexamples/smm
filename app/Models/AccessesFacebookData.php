<?php

namespace App\Models;

trait AccessesFacebookData
{
    /**
     * Возвращает данные facebook-лида в структурированном виде
     *
     * @return array
     */
    public function getFacebookData()
    {
        return json_decode($this->facebook_data, true);
    }

    /**
     * Задает новые данные facebook-лида в структурированном виде
     *
     * @param  array  $data
     * @return static
     */
    public function setFacebookData(array $data)
    {
        $this->facebook_data = json_encode($data);
        $this->save();

        return $this;
    }
}
