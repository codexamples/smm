<?php

namespace App\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;

class CallSchedule extends Model
{
    use BelongsToCallForm, AccessesFacebookData;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'hash';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hash',
        'lead_phone',
        'form_id',
        'facebook_data',
        'type',
        'call_back_count',
        'call_back_at',
    ];

    /**
     * Возвращает время перезвона
     *
     * @return \Carbon\Carbon
     */
    public function getAt()
    {
        return new Carbon($this->call_back_at);
    }

    /**
     * Задает время перезвона напрямую
     *
     * @param  \Carbon\Carbon  $at
     * @param  bool  $decrement_count
     * @return static
     */
    public function setAt(Carbon $at, bool $decrement_count)
    {
        $at->second = 0;

        if ($decrement_count) {
            $this->call_back_count--;
        }

        $this->call_back_at = $at;
        $this->save();

        return $this;
    }

    /**
     * Корректирует время перезвона, исходя из рабочего времени родительского колцентра
     * Кидает исключение, если не удалось этого сделать
     *
     * @return static
     * @throws \Exception
     */
    public function correctAt()
    {
        /** @var  \App\Models\CallForm  $form */
        $form = $this->form;

        /** @var  \App\Models\CallCenter  $center */
        $center = $form->center;

        $at = new Carbon($this->call_back_at);
        $n = 1;
        $save = false;

        while (true) {
            $n++;

            if ($n > 4320) {
                throw new Exception('Unable to correct the call back time');
            }

            if ($center->isInWorkingTimeNow($at)) {
                break;
            }

            $at->addMinute();
            $save = true;
        }

        return $save ? $this->setAt($at, false) : $this;
    }

    /**
     * Добавляет минуты к следующему перезвону, убавляет количество перезвонов
     * Кидает исключение, если после убавления, перезвонов не останется
     *
     * @param  int  $value
     * @param  bool  $decrement_count
     * @return static
     * @throws \Exception
     */
    public function addAtMinutes(int $value, bool $decrement_count)
    {
        if ($this->call_back_count === 0) {
            throw new Exception('No call backs left');
        }

        $at = new Carbon($this->call_back_at);
        $at->addMinutes($value);

        return $this->setAt($at, $decrement_count);
    }

    /**
     * Возвращает true, если у колцентра рабочее время, которое отсюда
     *
     * @param  \App\Models\CallCenter  $center
     * @return bool
     */
    public function isCenterInWorkingTimeNow(CallCenter $center)
    {
        return $center->isInWorkingTimeNow($this->getAt());
    }

    /**
     * Находит и возвращает запись отложенного вызова
     *
     * @param  string  $lead_phone
     * @param  int  $form_id
     * @return static|null
     */
    public static function findOne(string $lead_phone, int $form_id)
    {
        return static::where('lead_phone', $lead_phone)->where('form_id', $form_id)->first();
    }
}
