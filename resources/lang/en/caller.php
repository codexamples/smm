<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail sender lead dictionary
    |--------------------------------------------------------------------------
    |
    | The field names of the lead are translating by current dictionary
    | And if any field doesn't exist in current dictionary the original field
    | Name passes
    |
    */

    'mailgun' => [
        'page_name' => 'Page Name',
        'form_id' => 'Form ID',
        'form_name' => 'Form Name',
        'full_name' => 'Full Name',
        'email' => 'E-Mail',
        'phone_number' => 'Phone Number',
        'wants_call' => 'Wants call',
        'time_to_call' => 'Call time',
        'event' => 'Event',
    ],

    /*
    |--------------------------------------------------------------------------
    | Schedule statuses
    |--------------------------------------------------------------------------
    |
    | Status names of scheduled calls
    |
    */

    'schedules' => [
        'schedule' => 'Schedule',
        'unavailable_center' => 'Unavailable Call Center',
        'unavailable_lead' => 'Unavailable Lead',
        'custom' => 'Custom',
    ],

];
