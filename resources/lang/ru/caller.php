<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Словарь для почтовой рассылки о данных лида
    |--------------------------------------------------------------------------
    |
    | Названия полей переводятся согласно этому словарю, а если поля тут нет,
    | То выписывается оригинальное название
    |
    */

    'mailgun' => [
        'page_name' => 'Страница',
        'form_id' => 'ID формы',
        'form_name' => 'Форма',
        'full_name' => 'Имя',
        'email' => 'E-Mail',
        'phone_number' => 'Телефон',
        'wants_call' => 'Желает звонка',
        'time_to_call' => 'Время звонка',
        'event' => 'Событие',
    ],

    /*
    |--------------------------------------------------------------------------
    | Отложенные статусы
    |--------------------------------------------------------------------------
    |
    | Названия статусов для отложенных звонков
    |
    */

    'schedules' => [
        'schedule' => 'Отложен',
        'unavailable_center' => 'Колцентр недоступен',
        'unavailable_lead' => 'Лид недоступен',
        'custom' => 'Заданное время',
    ],

];
