<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="/favicon.ico">

        <title>Авторизация</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/css/core.css" rel="stylesheet" type="text/css">
        <link href="/css/icons.css" rel="stylesheet" type="text/css">
        <link href="/css/components.css" rel="stylesheet" type="text/css">
        <link href="/css/pages.css" rel="stylesheet" type="text/css">
        <link href="/css/menu.css" rel="stylesheet" type="text/css">
        <link href="/css/responsive.css" rel="stylesheet" type="text/css">

        <script src="/js/modernizr.min.js"></script>
        <script>
          window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
    </head>
    <body>
        <div class="wrapper-page">
            <div class="text-center">
                <div class="logo logo-lg">
                    <i class="md md-stars"></i>
                    <span>SMM-Stuff</span>
                </div>
            </div>

            <form class="form-horizontal m-t-20" action="{{ url('/login') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Логин" name="login">
                        <i class="md md-account-circle form-control-feedback l-h-34"></i>
                        @if ($errors->has('login'))
                            <span class="help-block">
                                <strong>{{ $errors->first('login') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Пароль" name="password">
                        <i class="md md-vpn-key form-control-feedback l-h-34"></i>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group text-right m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-custom w-md waves-effect waves-light" type="submit">Войти
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/detect.js"></script>
        <script src="/js/fastclick.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.blockUI.js"></script>
        <script src="/js/waves.js"></script>
        <script src="/js/wow.min.js"></script>
        <script src="/js/jquery.nicescroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>
        <script src="/js/jquery.core.js"></script>
        <script src="/js/jquery.app.js"></script>
    </body>
</html>
