<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li class="has_sub">
                    <a href="#" class="waves-effect waves-primary @if(Request::is('*gmparse*')) active @endif">
                        <i class="md md-map"></i>
                        <span>Парсер Google Maps</span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Request::is('*gmparse')) class="active" @endif>
                            <a href="{{ route('gmparse.index') }}">
                                <i class="md md-file-upload"></i>
                                <span>Загрузить</span>
                            </a>
                        </li>
                        <li @if(Request::is('*gmparse/status*')) class="active" @endif>
                            <a href="{{ route('gmparse.status') }}">
                                <i class="md md-alarm"></i>
                                <span>Статусы</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect waves-primary @if(Request::is('*call-*')) active @endif">
                        <i class="md md-call"></i>
                        <span>Звонилка</span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Request::is('*call-center*')) class="active" @endif>
                            <a href="{{ route('call_center.index') }}">
                                <i class="md md-quick-contacts-dialer"></i>
                                <span>Колцентры</span>
                            </a>
                        </li>
                        <li @if(Request::is('*call-form*')) class="active" @endif>
                            <a href="{{ route('call_form.index') }}">
                                <i class="md md-messenger"></i>
                                <span>Формы</span>
                            </a>
                        </li>
                        <li @if(Request::is('*call-msg-file*')) class="active" @endif>
                            <a href="{{ route('msg_file.index') }}">
                                <i class="md md-surround-sound"></i>
                                <span>Отбивки</span>
                            </a>
                        </li>
                        <li @if(Request::is('*call-log*')) class="active" @endif>
                            <a href="{{ route('call_log.index') }}">
                                <i class="md md-dehaze"></i>
                                <span>Журнал</span>
                            </a>
                        </li>
                        <li @if(Request::is('*call-schedule*')) class="active" @endif>
                            <a href="{{ route('call_schedule.index') }}">
                                <i class="md md-schedule"></i>
                                <span>Отложенные</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect waves-primary @if(Request::is('*tele-*')) active @endif">
                        <i class="md md-forum"></i>
                        <span>Telegram</span>
                    </a>
                    <ul class="list-unstyled">
                        <li @if(Request::is('*tele-chat*')) class="active" @endif>
                            <a href="{{ route('tele_chat.index') }}">
                                <i class="md md-message"></i>
                                <span>Чаты</span>
                            </a>
                        </li>
                        <li @if(Request::is('*tele-log*')) class="active" @endif>
                            <a href="{{ route('tele_log.index') }}">
                                <i class="md md-send"></i>
                                <span>Журнал</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
