@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('status'))
    <div class="alert alert-info">
        <ul>
            <li>{!! session('status') !!}</li>
        </ul>
    </div>
@endif

@if(session('msg'))
    <div class="alert alert-success">
        <ul>
            @if(is_array(session('msg')))
                @foreach(session('msg') as $msg)
                    <li>{!! $msg !!}</li>
                @endforeach
            @else
                <li>{!! session('msg') !!}</li>
            @endif
        </ul>
    </div>
@endif
