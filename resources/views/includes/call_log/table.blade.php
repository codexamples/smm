<div class="card-box">
    <div class="row">
        <div class="col-md-12">
            <h4 class="m-t-0 header-title"><b>Журнал</b></h4>
            <table class="table table-striped table-responsive">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Форма</th>
                    <th>Лид</th>
                    <th>Facebook</th>
                    <th>Статус</th>
                    <th>Запись</th>
                </tr>
                </thead>
                <tbody>
                @forelse($logs as $log)
                    <tr>
                        <td style="font-family: courier; word-break: keep-all;">
                            {{ date('Y-m-d', strtotime($log->frozen_at)) }}<br>
                            &nbsp;{{ date('H:i:s', strtotime($log->frozen_at)) }}
                        </td>
                        <td>
                            @if($log->form)
                                <a
                                        href="{{ route('call_form.edit', ['id' => $log->form->id]) }}"
                                        title="{{ $log->center_name }} ~ {{ $log->form_name }} ~ {{ $log->form->facebook_id }}"
                                >
                                    {{ $log->form->name }}
                                </a>
                                @if($log->form->mail_only)
                                    <br><i>Только E-Mail</i>
                                @endif
                                @if($log->form->chats()->count())
                                    <br><br>Telegram:
                                    @foreach($log->form->chats as $chat)
                                        <br>
                                        <a
                                                href="{{ route('tele_chat.view', ['id' => $chat->id]) }}"
                                                title="{{ $chat->name }} ({{ $chat->id }})"
                                        >
                                            {{ $chat->name }}
                                        </a>
                                    @endforeach
                                @endif
                            @else
                                <strong>{{ $log->center_name }}</strong>
                                <br>{{ $log->form_name }}
                            @endif
                            @if($log->center_phone && ($log->form && $log->form->center->phone !== $log->center_phone || !$log->form))
                                <br><br>Колцентр:
                                <br><i>{{ $log->center_phone }}</i>
                            @endif
                        </td>
                        <td>{{ $log->lead_phone }}</td>
                        <td>
                            @php
                                /** @var  \App\Models\CallLog  $log */
                                $data = $log->getFacebookData();

                                if (is_array($data)) {
                                    if (array_key_exists('token', $data)) {
                                        unset($data['token']);
                                    }
                                    if (array_key_exists('form_id', $data)) {
                                        unset($data['form_id']);
                                    }
                                    if (array_key_exists('phone_number', $data)) {
                                        unset($data['phone_number']);
                                    }
                                } else {
                                    $data = [];
                                }
                            @endphp
                            @forelse($data as $key => $value)
                                @if(!$loop->first)<br><br>@endif
                                <strong>{{ trans('caller.mailgun.' . $key) }}:</strong>
                                <br>{{ $value }}
                            @empty
                                <i>&lt; Нет &gt;</i>
                            @endforelse
                        </td>
                        <td>
                            @php
                                /** @var  \App\Models\CallLog  $log */
                                /** @var  \App\Business\Caller\Logging\Status  $status */

                                $status = $log->getStatus();
                                $style = $status->getViewStyle();
                                $text = $status->getViewText();
                                $css = [];

                                foreach ($style as $key => $value) {
                                    $css[] = $key . ': ' . $value . ';';
                                }
                            @endphp
                            <span class="label label-default" style="{{ implode(' ', $css) }}">{{ $text }}</span>
                        </td>
                        <td>
                            @if($log->record_path)
                                <audio src="{{ route('sound.record', ['id' => $log->id]) }}" controls></audio>
                            @else
                                <i>&lt; Нет &gt;</i>
                            @endif
                            <br><br>
                            <strong>Длительность:</strong>
                            <br>
                            @if($log->record_duration)
                                {{ floor($log->record_duration / 60) }}:{{ str_pad($log->record_duration % 60, 2, '0', STR_PAD_LEFT) }}
                            @else
                                --:--
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr><td colspan="6" style="text-align: center;">Пусто</td></tr>
                @endforelse
                </tbody>
            </table>

            {!! $logs->appends(['q' => $search])->links() !!}
        </div>
    </div>
</div>
