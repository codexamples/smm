<div class="row">
    <div class="col-md-12">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a
                                data-toggle="collapse"
                                data-parent="#accordion"
                                href="#filtering"
                                @if(!$search)
                                aria-expanded="false"
                                class="collapsed"
                                @endif
                        >
                            Фильтр
                        </a>
                    </h4>
                </div>
                <div id="filtering" class="panel-collapse collapse @if($search) in @endif">
                    <div class="panel-body">
                        <form method="get" action="{{ route('call_log.index') }}">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Дата</label>
                                    <div class="input-daterange input-group" id="date-range">
                                        <span class="input-group-addon bg-primary b-0 text-white">От</span>
                                        <input
                                                type="text"
                                                class="form-control"
                                                name="q[date_from]"
                                                value="{{ $search['date_from'] or '' }}"
                                        >
                                        <span class="input-group-addon bg-primary b-0 text-white">До</span>
                                        <input
                                                type="text"
                                                class="form-control"
                                                name="q[date_to]"
                                                value="{{ $search['date_to'] or '' }}"
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Формы</label>
                                    <select class="form-control select2" multiple name="q[form][]">
                                        @foreach($forms as $form)
                                            <option
                                                    value="{{ $form->id }}"
                                                    @if(is_array($search) && array_key_exists('form', $search) && is_array($search['form']) && in_array($form->id, $search['form'])) selected @endif
                                            >
                                                {{ $form->facebook_id }} ({{ $form->name }})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Лид</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="q[lead_phone]"
                                            value="{{ $search['lead_phone'] or '' }}"
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Facebook</label>
                                    <input
                                            type="text"
                                            class="form-control"
                                            name="q[facebook_data]"
                                            value="{{ $search['facebook_data'] or '' }}"
                                    >
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <button class="btn btn-purple pull-right" style="margin-top: 30px;">
                                        Поиск
                                    </button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('layout:js')
    <script type="text/javascript">
      $(document).ready(function () {
        $("#date-range").datepicker({
          toggleActive: true,
          autoclose: true,
          todayHighlight: true,
          format: 'yyyy-mm-dd'
        });
      });
    </script>
@endsection
