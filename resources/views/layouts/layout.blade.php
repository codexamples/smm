<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <link rel="shortcut icon" href="/favicon.ico">

        <title>SMM</title>

        <link href="/assets/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
        <link href="/assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="/assets/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />

        <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/css/core.css" rel="stylesheet" type="text/css">
        <link href="/css/icons.css" rel="stylesheet" type="text/css">
        <link href="/css/components.css" rel="stylesheet" type="text/css">
        <link href="/css/pages.css" rel="stylesheet" type="text/css">
        <link href="/css/menu.css" rel="stylesheet" type="text/css">
        <link href="/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="/plugins/select2/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="/plugins/select2/select2.css" rel="stylesheet" type="text/css">
        <link href="/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        @yield('layout:css')

        <script src="/assets/js/modernizr.min.js"></script>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @yield('layout:head_js')
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            @include('includes.topbar')

            @include('includes.left-sidebar')

            <div class="content-page">
                <div class="content">
                    <div class="container">
                        @include('includes.alerts')

                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <script>
            var resizefunc = [];
        </script>
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/detect.js"></script>
        <script src="/js/fastclick.js"></script>
        <script src="/js/jquery.slimscroll.js"></script>
        <script src="/js/jquery.blockUI.js"></script>
        <script src="/js/waves.js"></script>
        <script src="/js/wow.min.js"></script>
        <script src="/js/jquery.nicescroll.js"></script>
        <script src="/js/jquery.scrollTo.min.js"></script>
        <script src="/plugins/switchery/switchery.min.js"></script>
        <script src="/plugins/moment/moment.js"></script>
        <script src="/plugins/waypoints/lib/jquery.waypoints.js"></script>
        <script src="/plugins/counterup/jquery.counterup.min.js"></script>
        <script src="/plugins/sweetalert/dist/sweetalert.min.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.selection.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.stack.js"></script>
        <script src="/plugins/flot-chart/jquery.flot.crosshair.js"></script>
        <script src="/plugins/jquery-circliful/js/jquery.circliful.min.js"></script>
        <script src="/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
        <script src="/plugins/skyicons/skycons.min.js" type="text/javascript"></script>
        <script src="/assets/pages/jquery.todo.js"></script>
        <script src="/assets/pages/jquery.chat.js"></script>
        <script src="/assets/pages/jquery.dashboard.js"></script>
        <script src="/js/jquery.core.js"></script>
        <script src="/js/jquery.app.js"></script>
        <script src="/plugins/select2/select2.min.js"></script>
        <script src="/plugins/select2/select2_locale_ru.js"></script>
        <script src="/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
                $('.circliful-chart').circliful();
                $('.select2').select2();
            });

            /* BEGIN SVG WEATHER ICON */
            if (typeof Skycons !== 'undefined') {
                var icons = new Skycons(
                    {"color": "#00b19d"},
                    {"resizeClear": true}
                    ),
                    list  = [
                        "clear-day", "clear-night", "partly-cloudy-day",
                        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                        "fog"
                    ],
                    i;

                for(i = list.length; i--; )
                    icons.set(list[i], list[i]);
                icons.play();
            }
        </script>
        @yield('layout:js')
    </body>
</html>
