@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Отложенные звонки</b></h4>
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Форма</th>
                        <th>Лид</th>
                        <th>Facebook</th>
                        <th>Тип</th>
                        <th>Перезвон</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($schedules as $schedule)
                        <tr>
                            <td style="font-family: courier; word-break: keep-all;">
                                {{ date('Y-m-d', strtotime($schedule->created_at)) }}<br>
                                &nbsp;{{ date('H:i:s', strtotime($schedule->created_at)) }}
                            </td>
                            <td>
                                <a
                                        href="{{ route('call_form.edit', ['id' => $schedule->form->id]) }}"
                                        title="{{ $schedule->center_name }} ~ {{ $schedule->form_name }} ~ {{ $schedule->form->facebook_id }}"
                                >
                                    {{ $schedule->form->name }}
                                </a>
                                @if($schedule->form->chats()->count())
                                    <br><br>Telegram:
                                    @foreach($schedule->form->chats as $chat)
                                        <br>
                                        <a
                                                href="{{ route('tele_chat.view', ['id' => $chat->id]) }}"
                                                title="{{ $chat->name }} ({{ $chat->id }})"
                                        >
                                            {{ $chat->name }}
                                        </a>
                                    @endforeach
                                @endif
                            </td>
                            <td>{{ $schedule->lead_phone }}</td>
                            <td>
                                @php
                                    /** @var  \App\Models\CallLog  $log */
                                    $data = $schedule->getFacebookData();

                                    if (is_array($data)) {
                                        if (array_key_exists('token', $data)) {
                                            unset($data['token']);
                                        }
                                        if (array_key_exists('form_id', $data)) {
                                            unset($data['form_id']);
                                        }
                                        if (array_key_exists('phone_number', $data)) {
                                            unset($data['phone_number']);
                                        }
                                    } else {
                                        $data = [];
                                    }
                                @endphp
                                @forelse($data as $key => $value)
                                    @if(!$loop->first)<br><br>@endif
                                    <strong>{{ trans('caller.mailgun.' . $key) }}:</strong>
                                    <br>{{ $value }}
                                @empty
                                    <i>&lt; Нет &gt;</i>
                                @endforelse
                            </td>
                            <td>{{ trans('caller.schedules.' . $schedule->type) }}</td>
                            <td>
                                <div style="font-family: courier; word-break: keep-all;">
                                    {{ date('Y-m-d', strtotime($schedule->call_back_at)) }}<br>
                                    &nbsp;{{ date('H:i:s', strtotime($schedule->call_back_at)) }}
                                </div>
                                <br>
                                <strong>Перезвонов:</strong>
                                {{ $schedule->call_back_count }}
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="6" style="text-align: center;">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $schedules->links() !!}
            </div>
        </div>
    </div>
@endsection
