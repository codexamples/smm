@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-0 header-title"><b>Telegram-чат : <small>{{ $chat->name }}</small></b></h4>
            </div>
        </div>
    </div>

    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="header-title text-muted">Основа</h4><br>

                <div class="form-group">
                    <label>ID</label>
                    <div class="form-control">{{ $chat->id }}</div>
                </div>

                <div class="form-group">
                    <label>Название</label>
                    <div class="form-control">{{ $chat->name }}</div>
                </div>

                <div class="form-group">
                    <label>Дата</label>
                    <div class="form-control">{{ date('Y-m-d H:i:s', strtotime($chat->created_at)) }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="header-title text-muted">Дочерние</h4><br>

                <div class="form-group">
                    <label>Формы</label>
                    <ul>
                        @forelse($chat->forms as $form)
                            <li>
                                <a href="{{ route('call_form.edit', ['id' => $form->id]) }}">
                                    {{ $form->facebook_id }} ({{ $form->name }})
                                </a>
                            </li>
                        @empty
                            <li><i>&lt; Нет &gt;</i></li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
