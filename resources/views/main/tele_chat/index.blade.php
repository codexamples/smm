@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Telegram Channels</b></h4>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Название</th>
                        <th>Формы</th>
                        <th>Дата</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($chats as $chat)
                        <tr>
                            <th>{{ $chat->id }}</th>
                            <td>{{ $chat->name }}</td>
                            <td>{{ $chat->forms()->count() }}</td>
                            <td>{{ date('Y-m-d H:i:s', strtotime($chat->created_at)) }}</td>
                            <td>
                                <a
                                        href="{{ route('tele_chat.view', ['id' => $chat->id]) }}"
                                        class="btn btn-info"
                                ><i class="fa fa-search waves-effect waves-light"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="5" style="text-align: center;">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $chats->links() !!}
            </div>
        </div>
    </div>
@endsection
