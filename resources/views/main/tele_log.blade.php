@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Журнал</b></h4>
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Чат</th>
                        <th>Форма</th>
                        <th>Facebook</th>
                        <th>Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($logs as $log)
                        <tr>
                            <td style="font-family: courier; word-break: keep-all;">
                                {{ date('Y-m-d', strtotime($log->created_at)) }}<br>
                                &nbsp;{{ date('H:i:s', strtotime($log->created_at)) }}
                            </td>
                            <td>
                                @if($log->chat)
                                    <a
                                            href="{{ route('tele_chat.view', ['id' => $log->chat->id]) }}"
                                            title="{{ $log->telegram_name }} ({{ $log->chat->id }})"
                                    >
                                        {{ $log->chat->name }}
                                    </a>
                                @else
                                    <strong>{{ $log->telegram_name }}</strong>
                                    <br>{{ $log->telegram_id }}
                                @endif
                            </td>
                            <td>
                                @if($log->form)
                                    <a
                                            href="{{ route('call_form.edit', ['id' => $log->form->id]) }}"
                                            title="{{ $log->center_name }} ~ {{ $log->form_name }} ~ {{ $log->form->facebook_id }}"
                                    >
                                        {{ $log->form->name }}
                                    </a>
                                @else
                                    <strong>{{ $log->center_name }}</strong>
                                    <br>{{ $log->form_name }}
                                @endif
                            </td>
                            <td>
                                @php
                                    /** @var  \App\Models\TelegramLog  $log */
                                    $data = $log->getLeadData();

                                    if (is_array($data)) {
                                        $title = $data['title'];
                                        $data = $data['lead'];

                                        if (array_key_exists('token', $data)) {
                                            unset($data['token']);
                                        }
                                        if (array_key_exists('form_id', $data)) {
                                            unset($data['form_id']);
                                        }
                                    } else {
                                        $title = '!!!';
                                        $data = [];
                                    }
                                @endphp
                                <b>{{ $title }}</b>
                                @forelse($data as $key => $value)
                                    <br><br>{{ trans('caller.mailgun.' . $key) }}:<br><i>{{ $value }}</i>
                                @empty
                                    <i>&lt; Нет &gt;</i>
                                @endforelse
                            </td>
                            <td>
                                @if($log->error)
                                    <span class="label label-danger">{{ $log->error }}</span>
                                @else
                                    <span class="label label-success">Доставлено</span>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="5" style="text-align: center;">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $logs->links() !!}
            </div>
        </div>
    </div>
@endsection
