@extends('layouts.layout')

@section('content')
    @include('includes.call_log.search')
    @include('includes.call_log.table')
@endsection
