@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Формы</b></h4>
                <a
                        href="{{ route('call_form.create') }}"
                        class="btn btn-success"
                ><i class="fa fa-plus waves-effect waves-light"></i> Добавить
                </a><br><br>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Форма</th>
                        <th>Колцентр</th>
                        <th>Telegram Chats</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($forms as $form)
                        <tr>
                            <th>{{ $form->facebook_id }}</th>
                            <th>{{ $form->name }}</th>
                            <td>
                                <a href="{{ route('call_center.edit', ['id' => $form->center->id]) }}">
                                    {{ $form->center->name }} ({{ $form->center->phone }})
                                </a>
                            </td>
                            <td>{{ $form->chats()->count() }}</td>
                            <td>
                                <a
                                        href="{{ route('call_form.edit', ['id' => $form->id]) }}"
                                        class="btn btn-info"
                                ><i class="fa fa-pencil waves-effect waves-light"></i>
                                </a>
                                <a
                                        href="{{ route('call_form.destroy', ['id' => $form->id]) }}"
                                        class="btn btn-danger"
                                        onclick="event.preventDefault(); if (confirm('Вы уверены в этом?')) document.getElementById('df-{{ $form->id }}').submit();"
                                ><i class="fa fa-trash waves-effect waves-light"></i>
                                </a>
                                <form
                                        method="post"
                                        action="{{ route('call_form.destroy', ['id' => $form->id]) }}"
                                        style="display: none;"
                                        id="df-{{ $form->id }}"
                                >
                                    {{ csrf_field() }}
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="5" style="text-align: center;">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $forms->links() !!}
            </div>
        </div>
    </div>
@endsection
