@extends('layouts.layout')

@section('content')
    <form
            role="form"
            method="POST"
            action="{{ $form ? route('call_form.update', ['id' => $form->id]) : route('call_form.store') }}"
    >
        {{ csrf_field() }}

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="m-0 header-title pull-left">
                        <b>
                            @if($form)
                                Форма : <small>{{ $form->name }}</small>
                            @else
                                Новая форма
                            @endif
                        </b>
                    </h4>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Основа</h4><br>

                    <div class="form-group">
                        <label>#</label>
                        <input
                                type="text"
                                name="facebook_id"
                                class="form-control"
                                value="{{ $form->facebook_id or old('facebook_id') }}"
                                placeholder="1005001488"
                        >
                    </div>

                    <div class="form-group">
                        <label>Название</label>
                        <input
                                type="text"
                                name="name"
                                class="form-control"
                                value="{{ $form->name or old('name') }}"
                                placeholder="Top Форма In Da World"
                        >
                    </div>

                    <div class="form-group">
                        <label>Колцентр</label>
                        <select class="form-control" name="center_id">
                            @foreach($centers as $center)
                                <option value="{{ $center->id }}" @if($form && $form->center_id === $center->id || old('center_id') === $center->id) selected @endif>
                                    {{ $center->name }} ({{ $center->phone }})
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">E-Mail</h4><br>

                    <br><div class="form-group">
                        <h4>Не звонить, а только оповещать по E-Mail</h4>
                        @foreach(['Нет', 'Да'] as $index => $item)
                            <label>
                                <input
                                        type="radio"
                                        name="mail_only"
                                        value="{{ $index }}"
                                        style="vertical-align: bottom;"
                                        @if($form && $form->mail_only == $index || old('mail_only') == $index) checked @endif
                                > {{ $item }}
                            </label>
                        @endforeach
                    </div><br>

                    <div class="form-group">
                        <label>Отправка почты из:</label>
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        name="from_mail"
                                        class="form-control"
                                        value="{{ $form->from_mail or old('from_mail') }}"
                                        placeholder="{{ config('mailgun.from.address') }}"
                                >
                            </div>
                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        name="from_name"
                                        class="form-control"
                                        value="{{ $form->from_name or old('from_name') }}"
                                        placeholder="{{ config('mailgun.from.name') }}"
                                >
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Telegram</h4><br>

                    <div class="form-group">
                        <label>Чаты</label>
                        @forelse($chats as $chat)
                            <label style="font-weight: 200; display: block;">
                                <input
                                        style="vertical-align: bottom;"
                                        type="checkbox"
                                        name="chats[]"
                                        value="{{ $chat->id }}"
                                        @if(in_array($chat->id, $form_chat)) checked @endif
                                >
                                {{ $chat->name }} ({{ $chat->id }})
                            </label>
                        @empty
                            <i>Пусто</i>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">SMS</h4><br>

                    <div class="form-group">
                        <label>Отправитель</label>
                        <input
                                type="text"
                                name="sms_sender"
                                class="form-control"
                                value="{{ $form->sms_sender or old('sms_sender') }}"
                                placeholder="{{ config('caller.sms.sender') }}"
                        >
                    </div>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-inverse text-white">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a
                                            data-toggle="collapse"
                                            data-parent="#accordion"
                                            href="#filtering"
                                            aria-expanded="false"
                                            class="collapsed"
                                    >
                                        !!! ПОМОЩЬ ПО ШАБЛОНАМ !!!
                                    </a>
                                </h4>
                            </div>
                            <div id="filtering" class="panel-collapse collapse">
                                <div class="panel-body text-inverse">
                                    <p>Простой текст, с дополнением:</p>
                                    <ul>
                                        <li>%n% - перенос на новую строку</li>
                                        <li>%формат% - дата / время перезвона</li>
                                    </ul>
                                    <p><i><u>Формат</u></i> подразумевает специальные символы, на места которых подставляются компоненты даты и времени. Вот некоторые из них:</p>
                                    <ul>
                                        <li>m - числовой месяц <i>[01-12]</i></li>
                                        <li>d - день месяца <i>[01-31]</i></li>
                                        <li>H - час <i>[00-23]</i></li>
                                        <li>i - минута <i>[00-59]</i></li>
                                    </ul>
                                    <p>Это далеко не все компоненты, остальные доступны <a href="http://php.net/manual/ru/function.date.php" target="_blank"><b>здесь</b></a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Текст при простом перезвоне</label>
                        <div class="input-group">
                            <input
                                    type="text"
                                    name="sms_text_simple"
                                    class="form-control"
                                    value="{{ $form->sms_text_simple or old('sms_text_simple') }}"
                                    placeholder="{{ config('caller.sms.texts.simple') }}"
                            >
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary sms-preview">
                                    <i class="md md-textsms"></i>
                                </button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Текст при отложенном перезвоне</label>
                        <div class="input-group">
                            <input
                                    type="text"
                                    name="sms_text_schedule"
                                    class="form-control"
                                    value="{{ $form->sms_text_schedule or old('sms_text_schedule') }}"
                                    placeholder="{{ config('caller.sms.texts.schedule') }}"
                            >
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary sms-preview">
                                    <i class="md md-textsms"></i>
                                </button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Текст при назначенном времени в форме</label>
                        <div class="input-group">
                            <input
                                    type="text"
                                    name="sms_text_time"
                                    class="form-control"
                                    value="{{ $form->sms_text_time or old('sms_text_time') }}"
                                    placeholder="{{ config('caller.sms.texts.time') }}"
                            >
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary sms-preview">
                                    <i class="md md-textsms"></i>
                                </button>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Текст при неверно назначенном времени в форме</label>
                        <div class="input-group">
                            <input
                                    type="text"
                                    name="sms_text_time_invalid"
                                    class="form-control"
                                    value="{{ $form->sms_text_time_invalid or old('sms_text_time_invalid') }}"
                                    placeholder="{{ config('caller.sms.texts.time_invalid') }}"
                            >
                            <span class="input-group-btn">
                                <button type="button" class="btn waves-effect waves-light btn-primary sms-preview">
                                    <i class="md md-textsms"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-purple waves-effect waves-light">
                        {{ $form ? 'Обновить' : 'Создать' }}
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('layout:js')
    <script type="text/javascript">
        $(document).ready(function () {
          $('.sms-preview').on('click', function () {
            var input = $(this).parent().parent().find('input[type=text]');
            var text = input.val() === '' ? input.prop('placeholder') : input.val();

            $.ajax({
              url: '/api/get-sms-text',
              type: 'post',
              data: text,
              success: function (data) {
                alert(data);
              },
              error: function (xhr, err, msg) {
                alert('Ошибка:\n\n' + msg);
              }
            });
          });
        });
    </script>
@endsection
