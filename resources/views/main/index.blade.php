@extends('layouts.layout')

@section('content')
    @php
    $texts = [
        'Вместо пасхалки этот текст',
        'Приветствуем Вас на борту <b>Boeing SMM</b>',
        '♥',
    ];
    @endphp
    <div class="card-box">
        <div class="row" style="text-align: center; font-size: 20px; font-family: courier new;" id="garland">
            {!! $texts[rand(0, count($texts) - 1)] !!}
        </div>
    </div>
@endsection

@section('layout:js')
    <script type="text/javascript">
        garland = {
        };

        $(document).ready(function () {
          var a = $("#garland").html();
          var b = "";
          var tag = false;
          var tagval = "";

          for (var i = 0; i < a.length; i++) {
            var ch = a.substr(i, 1);

            if (ch == "<") {
              tag = true;
            } else if (ch == ">") {
              tag = false;
              b += "<" + tagval + ">";
              tagval = "";
            } else if (tag) {
              tagval += ch;
            } else {
              if (a.charCodeAt(i) < 48) {
                b += ch;
              } else {
                b += "<span class='garland-ch'>" + ch + "</span>";
              }
            }
          }

          $("#garland").html(b);
          garland.step(0, null);
        });

        garland.step = function (mode, args) {
          switch (mode) {
            case 0:
              var colors = ["#a00", "#0a0", "#00a", "#aa0", "#0aa", "#a0a"];
              var color = colors[Math.floor(Math.random() * colors.length)];
              var func = function (index, item) {
                $(item).css("color", color).css("opacity", 0.0);
              };
              var after = null;

              mode = 1;
              args = {
                increase: true,
                position: 0
              };
              break;

            case 1:
              var count = 0;
              var func = function (index, item) {
                count++;

                if (index == args.position || !args.increase) {
                  var opacity = parseFloat($(item).css("opacity"));

                  if (args.increase) {
                    opacity += 0.04;

                    if (opacity >= 1.0) {
                      opacity = 1.0;
                      args.position++;
                    }
                  } else {
                    opacity -= 0.02;

                    if (opacity <= 0.0) {
                      opacity = 0.0;
                      args.position = count;
                    }
                  }

                  $(item).css("opacity", opacity);
                }
              };
              var after = function () {
                if (args.position >= count) {
                  args.position = 0;
                  args.increase = !args.increase;

                  if (args.increase) {
                    mode = 0;
                    args = null;
                  }
                }
              };
              break;
          }

          $(".garland-ch").each(func);

          if (after) {
            after();
          }

          setTimeout(function () {
            garland.step(mode, args);
          }, 10);
        };
    </script>
@endsection
