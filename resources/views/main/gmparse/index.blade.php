@extends('layouts.layout')

@section('content')
    <form role="form" method="POST" action="{{ route('gmparse.upload') }}" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="m-0 header-title">
                        <b>Загрузка координат Google Maps</b>
                    </h4>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning">
                        <p>У Google Maps API имеется органичение на дневное количество запросов.</p>
                        <p>Ошибка <span class="label label-danger">You have exceeded your daily request quota for this API.</span> сообщает именно о превышении дневной нормы запросов.</p>
                        <p>В случае выполнения дневной нормы запросов, пока что, придется ждать до завтра...</p>
                    </div>

                    <div class="form-group">
                        <label>Файл с координатами (TXT)</label>
                        <input type="file" name="file" class="form-control">
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-purple waves-effect waves-light">Загрузить</button>
                </div>
            </div>
        </div>
    </form>
@endsection
