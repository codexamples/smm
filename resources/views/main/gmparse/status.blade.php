@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Статусы координат Google Maps</b></h4>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ids = []; ?>
                        @forelse($gmparses as $gmparse)
                            <?php $ids[] = $gmparse->id; ?>
                            <tr>
                                <th>{{ $gmparse->name }}</th>
                                <td class="item--status id--{{ $gmparse->id }}"><i>Загрузка...</i></td>
                                <td>
                                    <button
                                            class="btn btn-primary item--waiting id--{{ $gmparse->id }} disabled"
                                    >...
                                    </button>
                                    <a
                                            href="{{ route('gmparse.download', ['id' => $gmparse->id]) }}"
                                            class="btn btn-info item--download id--{{ $gmparse->id }}"
                                            target="_blank"
                                            style="display: none;"
                                    ><i class="fa fa-download waves-effect waves-light"></i>
                                    </a>
                                    <a
                                            href="{{ route('gmparse.retry', ['id' => $gmparse->id]) }}"
                                            class="btn btn-warning item--retry id--{{ $gmparse->id }}"
                                            style="display: none;"
                                    ><i class="fa fa-refresh waves-effect waves-light"></i>
                                    </a>
                                    <a
                                            href="{{ route('gmparse.delete', ['id' => $gmparse->id]) }}"
                                            class="btn btn-danger item--delete id--{{ $gmparse->id }}"
                                            style="display: none;"
                                            onclick="return confirm('Вы уверены?');"
                                    ><i class="fa fa-trash waves-effect waves-light"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr><td colspan="3" style="text-align: center;">Пусто</td></tr>
                        @endforelse
                    </tbody>
                </table>

                {!! $gmparses->links() !!}
            </div>
        </div>
    </div>
@endsection

@section('layout:js')
    <script type="text/javascript">
        $(document).ready(function () {
          {{ json_encode($ids) }}.map(processStatusID);
        });

        window.processStatusID = function (id) {
          var el = {
            status: $('.item--status.id--' + id),
            waiting: $('.item--waiting.id--' + id),
            download: $('.item--download.id--' + id),
            retry: $('.item--retry.id--' + id),
            remove: $('.item--delete.id--' + id)
          };

          setTimeout(function () {
            $.ajax({
              url: '/api/gmparse/status/' + id,
              type: 'get',

              error: function (xhr, msg, err) {
                el.status.html('<span class="text-danger">' + msg + '</span>');
                processStatusID(id);
              },

              success: function (data) {
                var again = true;

                if (data.hasOwnProperty('raw')) {
                  switch (data.raw) {
                    case '':
                      el.status.html('<span class="text-muted">Ожидается</span>');
                      break;

                    case 'ready':
                      el.status.html('<span class="label label-success">Готово</span>');
                      el.waiting.hide();
                      el.download.show();
                      el.remove.show();

                      again = false;
                      break;
                  }
                } else if (data.hasOwnProperty('type')) {
                  switch (data.type) {
                    case 'parsing':
                      el.status.html('<span class="label label-info">' + data.parsed + ' / ' + data.all + '</span>');
                      break;

                    case 'error':
                      el.status.html('<span class="label label-danger">' + data.msg + '</span>');
                      el.waiting.hide();
                      el.retry.on('click', function (e) {
                        e.preventDefault();

                        if (confirm('Вы уверены?')) {
                          var form = $('<form method="POST" action="' + el.retry.prop('href') + '">{{ csrf_field() }}</form>');
                          $('body').append(form);
                          form.submit();
                        }
                      }).show();
                      el.remove.show();

                      again = false;
                      break;
                  }
                }

                if (again) {
                  processStatusID(id);
                }
              }
            });
          }, 250);
        }
    </script>
@endsection
