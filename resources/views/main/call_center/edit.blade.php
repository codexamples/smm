@extends('layouts.layout')

@section('content')
    <form
            role="form"
            method="POST"
            action="{{ $center ? route('call_center.update', ['id' => $center->id]) : route('call_center.store') }}"
            enctype="multipart/form-data"
    >
        {{ csrf_field() }}

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="m-0 header-title">
                        <b>
                            @if(isset($center))
                                Колцентр : <small>{{ $center->name }}</small>
                            @else
                                Новый колцентр
                            @endif
                        </b>
                    </h4>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Основа</h4><br>

                    <div class="form-group">
                        <label>Название</label>
                        <input
                                type="text"
                                name="name"
                                class="form-control"
                                value="{{ $center->name or old('name') }}"
                                placeholder="Caller Corp."
                        >
                    </div>

                    <div class="form-group">
                        <label>Телефон</label>
                        <input
                                type="text"
                                name="phone"
                                class="form-control"
                                value="{{ $center->phone or old('phone') }}"
                                placeholder="78005553535"
                        >
                    </div>

                    <div class="form-group">
                        <label>Отбивка</label>
                        <select class="form-control" name="msg_file_hash">
                            <option value="">&lt; По умолчанию &gt;</option>
                            @foreach($msg_files as $file)
                                <option
                                        value="{{ $file->hash }}"
                                        @if($center && $center->msg_file_hash === $file->hash || old('msg_file_hash') && old('msg_file_hash') === $file->hash) selected @endif
                                >
                                    {{ $file->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Задержка перед отбивкой в секундах</label>
                        <input
                                type="text"
                                name="msg_delay"
                                class="form-control"
                                value="{{ $center->msg_delay or old('msg_delay') }}"
                                placeholder=""
                        >
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Рабочие часы</h4><br>

                    <div class="form-group">
                        <label>Время работы</label>
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        name="works_from"
                                        class="form-control"
                                        @if($center && $center->works_from)
                                        value="{{ date('H:i', strtotime($center->works_from)) }}"
                                        @elseif(old('works_from'))
                                        value="{{ date('H:i', strtotime(old('works_from'))) }}"
                                        @else
                                        value=""
                                        @endif
                                        placeholder="{{ date('H:i', strtotime(config('caller.working_time.from'))) }}"
                                >
                            </div>
                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        name="works_to"
                                        class="form-control"
                                        @if($center && $center->works_to)
                                        value="{{ date('H:i', strtotime($center->works_to)) }}"
                                        @elseif(old('works_to'))
                                        value="{{ date('H:i', strtotime(old('works_to'))) }}"
                                        @else
                                        value=""
                                        @endif
                                        placeholder="{{ date('H:i', strtotime(config('caller.working_time.to'))) }}"
                                >
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <br><div class="form-group">
                        <h4>Работает в выходные</h4>
                        @foreach(['Нет', 'Да'] as $index => $item)
                            <label>
                                <input
                                        type="radio"
                                        name="works_weekends"
                                        value="{{ $index }}"
                                        style="vertical-align: bottom;"
                                        @if($center && $center->works_weekends == $index || old('works_weekends') == $index) checked @endif
                                > {{ $item }}
                            </label>
                        @endforeach
                    </div><br>

                    <div class="form-group">
                        <label>Время работы в выходные</label>
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        name="works_weekends_from"
                                        class="form-control"
                                        @if($center && $center->works_weekends_from)
                                        value="{{ date('H:i', strtotime($center->works_weekends_from)) }}"
                                        @elseif(old('works_weekends_from'))
                                        value="{{ date('H:i', strtotime(old('works_weekends_from'))) }}"
                                        @else
                                        value=""
                                        @endif
                                        placeholder="{{ date('H:i', strtotime(config('caller.working_time.from'))) }}"
                                >
                            </div>
                            <div class="col-lg-6">
                                <input
                                        type="text"
                                        name="works_weekends_to"
                                        class="form-control"
                                        @if($center && $center->works_weekends_to)
                                        value="{{ date('H:i', strtotime($center->works_weekends_to)) }}"
                                        @elseif(old('works_weekends_to'))
                                        value="{{ date('H:i', strtotime(old('works_weekends_to'))) }}"
                                        @else
                                        value=""
                                        @endif
                                        placeholder="{{ date('H:i', strtotime(config('caller.working_time.to'))) }}"
                                >
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Оповещение</h4><br>

                    <div class="form-group">
                        <label>Клиентские почты</label>
                        <input
                                type="text"
                                name="client_mails"
                                class="form-control"
                                value="{{ $center->client_mails or old('client_mails') }}"
                                placeholder="{{ 'aa@aa.ru,bb@bb.com,cc@cc.net' }}"
                        >
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Перезвон</h4><br>

                    <div class="form-group">
                        <h4>Перезванивать, если лид недоступен</h4>
                        @foreach(['Нет', 'Да'] as $index => $item)
                            <label>
                                <input
                                        type="radio"
                                        name="calls_back"
                                        value="{{ $index }}"
                                        style="vertical-align: bottom;"
                                        @if($center && $center->calls_back == $index || old('calls_back') == $index) checked @endif
                                > {{ $item }}
                            </label>
                        @endforeach
                    </div><br>

                    <div class="form-group">
                        <label>Количество перезвонов</label>
                        <input
                                type="text"
                                name="calls_back_times"
                                class="form-control"
                                value="{{ $center->calls_back_times or old('calls_back_times') }}"
                                placeholder="{{ config('caller.calls_back.times') }}"
                        >
                    </div>

                    <div class="form-group">
                        <label>Интервал в минутах</label>
                        <input
                                type="text"
                                name="calls_back_interval"
                                class="form-control"
                                value="{{ $center->calls_back_interval or old('calls_back_interval') }}"
                                placeholder="{{ config('caller.calls_back.interval') }}"
                        >
                    </div>
                </div>
            </div>
        </div>

        @if($center)
            <div class="card-box">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="header-title text-muted">Дочерние</h4><br>

                        <div class="form-group">
                            <label>Формы</label>
                            <ul>
                                @forelse($center->forms as $form)
                                    <li>
                                        <a href="{{ route('call_form.edit', ['id' => $form->id]) }}">
                                            {{ $form->facebook_id }} ({{ $form->name }})
                                        </a>
                                    </li>
                                @empty
                                    <li><i>&lt; Нет &gt;</i></li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="card-box">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-purple waves-effect waves-light">
                        {{ $center ? 'Обновить' : 'Создать' }}
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection
