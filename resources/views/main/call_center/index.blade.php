@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Колцентры</b></h4>
                <a
                        href="{{ route('call_center.create') }}"
                        class="btn btn-success"
                ><i class="fa fa-plus waves-effect waves-light"></i> Добавить
                </a><br><br>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Телефон</th>
                        <th>Формы</th>
                        <th>Отбивка</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($centers as $center)
                        <tr>
                            <th>{{ $center->name }}</th>
                            <td>{{ $center->phone }}</td>
                            <td>{{ $center->forms()->count() }}</td>
                            <td>
                                @if($center->msgFile)
                                    <audio src="{{ route('sound.msg_file', ['hash' => $center->msgFile->hash]) }}" controls></audio>
                                @else
                                    <i>&lt; По умолчанию &gt;</i>
                                @endif
                            </td>
                            <td>
                                <a
                                        href="{{ route('call_center.edit', ['id' => $center->id]) }}"
                                        class="btn btn-info"
                                ><i class="fa fa-pencil waves-effect waves-light"></i>
                                </a>
                                <a
                                        href="{{ route('call_center.destroy', ['id' => $center->id]) }}"
                                        class="btn btn-danger"
                                        onclick="event.preventDefault(); if (confirm('Вы уверены в этом?')) document.getElementById('df-{{ $center->id }}').submit();"
                                ><i class="fa fa-trash waves-effect waves-light"></i>
                                </a>
                                <form
                                        method="post"
                                        action="{{ route('call_center.destroy', ['id' => $center->id]) }}"
                                        style="display: none;"
                                        id="df-{{ $center->id }}"
                                >
                                    {{ csrf_field() }}
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="5" style="text-align: center;">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $centers->links() !!}
            </div>
        </div>
    </div>
@endsection
