@extends('layouts.layout')

@section('content')
    <div class="card-box">
        <div class="row">
            <div class="col-md-12">
                <h4 class="m-t-0 header-title"><b>Отбивки</b></h4>
                <a
                        href="{{ route('msg_file.create') }}"
                        class="btn btn-success"
                ><i class="fa fa-plus waves-effect waves-light"></i> Добавить
                </a><br><br>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Колцентры</th>
                        <th>Прослушать</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($files as $file)
                        <tr>
                            <th>{{ $file->name }}</th>
                            <td>{{ $file->centers()->count() }}</td>
                            <td><audio src="{{ route('sound.msg_file', ['hash' => $file->hash]) }}" controls></audio></td>
                            <td>
                                <a
                                        href="{{ route('msg_file.edit', ['hash' => $file->hash]) }}"
                                        class="btn btn-info"
                                ><i class="fa fa-pencil waves-effect waves-light"></i>
                                </a>
                                <a
                                        href="{{ route('msg_file.destroy', ['hash' => $file->hash]) }}"
                                        class="btn btn-danger"
                                        onclick="event.preventDefault(); if (confirm('Вы уверены в этом?')) document.getElementById('df-{{ $file->hash }}').submit();"
                                ><i class="fa fa-trash waves-effect waves-light"></i>
                                </a>
                                <form
                                        method="post"
                                        action="{{ route('msg_file.destroy', ['hash' => $file->hash]) }}"
                                        style="display: none;"
                                        id="df-{{ $file->hash }}"
                                >
                                    {{ csrf_field() }}
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr><td colspan="4" style="text-align: center;">Пусто</td></tr>
                    @endforelse
                    </tbody>
                </table>

                {!! $files->links() !!}
            </div>
        </div>
    </div>
@endsection
