@extends('layouts.layout')

@section('content')
    <form
            role="form"
            method="POST"
            action="{{ $file ? route('msg_file.update', ['hash' => $file->hash]) : route('msg_file.store') }}"
            enctype="multipart/form-data"
    >
        {{ csrf_field() }}

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="m-0 header-title pull-left">
                        <b>
                            @if($file)
                                Отбивка : <small>{{ $file->name }}</small>
                            @else
                                Новая отбивка
                            @endif
                        </b>
                    </h4>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Основа</h4><br>

                    <div class="form-group">
                        <label>Название</label>
                        <input
                                type="text"
                                name="name"
                                class="form-control"
                                value="{{ $file->name or old('name') }}"
                                placeholder="Sample Msg File"
                        >
                    </div>
                </div>
            </div>
        </div>

        <div class="card-box">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="header-title text-muted">Звук</h4><br>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-inverse text-white">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a
                                            data-toggle="collapse"
                                            data-parent="#accordion"
                                            href="#filtering"
                                            aria-expanded="false"
                                            class="collapsed"
                                    >
                                        !!! ИНСТРУКЦИЯ !!!
                                    </a>
                                </h4>
                            </div>
                            <div id="filtering" class="panel-collapse collapse">
                                <div class="panel-body text-inverse">
                                    <ol>
                                        <li>Берем любой звуковой файл</li>
                                        <li>
                                            Заходим
                                            <a href="http://audio.online-convert.com/ru/convert-to-wav" target="_blank"><b>сюда</b></a>
                                        </li>
                                        <li>Указываем файл в форме, либо по прямой ссылке</li>
                                        <li>
                                            Указываем параметры:
                                            <ul>
                                                <li>Битрейт: <strong>16 бит</strong></li>
                                                <li>Частота дискретизации: <strong>8000 герц</strong></li>
                                                <li>Аудио-каналы: <strong>моно</strong></li>
                                            </ul>
                                        </li>
                                        <li>Жмем <i><u>Преобразовать файл</u></i></li>
                                        <li>Полученный файл можно загружать в эту форму</li>
                                        <li>???</li>
                                        <li>PROFIT !!!</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            Файл
                            <span class="label label-default m-l-5">WAV [8 kHz] [8 bit] [Mono]</span>
                        </label>
                        <input type="file" name="file" class="form-control">
                        @if($file)
                            <div class="m-t-10">
                                <audio src="{{ route('sound.msg_file', ['hash' => $file->hash]) }}" controls></audio>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @if($file)
            <div class="card-box">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="header-title text-muted">Дочерние</h4><br>

                        <div class="form-group">
                            <label>Колцентры</label>
                            <ul>
                                @forelse($file->centers as $center)
                                    <li>
                                        <a href="{{ route('call_center.edit', ['id' => $center->id]) }}">
                                            {{ $center->name }} ({{ $center->phone }})
                                        </a>
                                    </li>
                                @empty
                                    <li><i>&lt; Нет &gt;</i></li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="card-box">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-purple waves-effect waves-light">
                        {{ $file ? 'Обновить' : 'Создать' }}
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection
