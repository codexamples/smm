## SMM ##
`PHP 7.0`
`Laravel 5.4`
`MySQL 5.7`

## Архитектура ##
#### Паблик в Facebook / Instagram ####
* Отправка формы в проект

#### SMM-Stuff ####
* Отправка данных лида клиенту через почту
* Отправка данных лида в _Telegram_
* Генерация звонка через _Asterisk_
* Отправка _SMS_-сообщения лиду

## Подвязать форму Facebook / Instagram ##
* Создать форму в паблике _Facebook_ / _Instagram_
* Добавить пользователя https://www.facebook.com/profile.php?id=*** в администраторы паблика
* Узнать _ID_ формы
* Зайти в _SMM_, раздел **Звонилка > Формы**
* Создать новую запись в таблице форм, где _ID_ будет совпадать с _ID_ формы в паблике _Facebook / Instagram_, остальные поля заполняются в соответствии с требованиями и данными от клиента

## Отправка тестового лида ##
https://developers.facebook.com/tools/lead-ads-testing?locale=ru_RU

## Дополнительные поля на формах Facebook / Instagram ##
#### Перезвонить ли Вам ####
* Название: `wants_call`
* Варианты ответа: Да - `yes`; Нет - `no`

#### Укажите, в какое время Вам перезвонить ####
* Название: `call_time`
* Тип: тестовое поле
* Формат ответа: **ЧЧ:ММ**
