<?php

Route::group(['prefix' => 'gmparse'], function () {
    Route::get('status/{id}', 'Api\GmparseController@status');
});

Route::group(['prefix' => 'call'], function () {
    Route::post('generate', 'Api\CallController@generate');
    Route::get('end', 'Api\CallController@end');
    Route::get('record', 'Api\CallController@record');
});

Route::post('/***', 'Api\TelegramController@webhook');

Route::post('/get-sms-text', 'Api\IndexController@getSmsText');
