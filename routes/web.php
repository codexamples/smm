<?php

Route::get('login', 'LoginController@showLoginForm')->name('login');
Route::post('login', 'LoginController@login');
Route::post('logout', 'LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'IndexController@index')->name('index');

    Route::group(['prefix' => 'gmparse'], function () {
        Route::get('/', 'GmparseController@index')->name('gmparse.index');
        Route::post('/upload', 'GmparseController@upload')->name('gmparse.upload');
        Route::get('/status', 'GmparseController@status')->name('gmparse.status');
        Route::get('/download/{id}', 'GmparseController@download')->name('gmparse.download');
        Route::post('/retry/{id}', 'GmparseController@retry')->name('gmparse.retry');
        Route::get('/delete/{id}', 'GmparseController@delete')->name('gmparse.delete');
    });

    Route::group(['prefix' => 'call-center'], function () {
        Route::get('/', 'CallCenterController@index')->name('call_center.index');
        Route::get('/create', 'CallCenterController@create')->name('call_center.create');
        Route::get('/{id}', 'CallCenterController@edit')->name('call_center.edit');
        Route::post('/store', 'CallCenterController@store')->name('call_center.store');
        Route::post('/update/{id}', 'CallCenterController@update')->name('call_center.update');
        Route::post('/destroy/{id}', 'CallCenterController@destroy')->name('call_center.destroy');
    });

    Route::group(['prefix' => 'call-form'], function () {
        Route::get('/', 'CallFormController@index')->name('call_form.index');
        Route::get('/create', 'CallFormController@create')->name('call_form.create');
        Route::get('/{id}', 'CallFormController@edit')->name('call_form.edit');
        Route::post('/store', 'CallFormController@store')->name('call_form.store');
        Route::post('/update/{id}', 'CallFormController@update')->name('call_form.update');
        Route::post('/destroy/{id}', 'CallFormController@destroy')->name('call_form.destroy');
    });

    Route::group(['prefix' => 'call-msg-file'], function () {
        Route::get('/', 'MsgFileController@index')->name('msg_file.index');
        Route::get('/create', 'MsgFileController@create')->name('msg_file.create');
        Route::get('/{hash}', 'MsgFileController@edit')->name('msg_file.edit');
        Route::post('/store', 'MsgFileController@store')->name('msg_file.store');
        Route::post('/update/{hash}', 'MsgFileController@update')->name('msg_file.update');
        Route::post('/destroy/{hash}', 'MsgFileController@destroy')->name('msg_file.destroy');
    });

    Route::get('call-log', 'CallLogController@index')->name('call_log.index');
    Route::get('call-schedule', 'CallScheduleController@index')->name('call_schedule.index');

    Route::group(['prefix' => 'tele-chat'], function () {
        Route::get('/', 'TeleChatController@index')->name('tele_chat.index');
        Route::get('/{id}', 'TeleChatController@view')->name('tele_chat.view');
    });

    Route::get('tele-log', 'TeleLogController@index')->name('tele_log.index');

    Route::group(['prefix' => 'sound'], function () {
        Route::get('record/{id}', 'SoundController@record')->name('sound.record');
        Route::get('msg-file/{hash}', 'SoundController@msgFile')->name('sound.msg_file');
    });
});
